package com.kovid.a1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SolutionTests {

    Data data60Students, data120Students, data240Students, data500Students;
    Solution sol60, sol120, sol240, sol500;

    List<Solution> solutions;

    public SolutionTests() throws IOException{

        File students60File = new File("src/main/resources/sampleSize60Students.xlsx");
        File students120File = new File("src/main/resources/sampleSize120Students.xlsx");
        File students240File = new File("src/main/resources/sampleSize240Students.xlsx");
        File students500File = new File("src/main/resources/sampleSize500Students.xlsx");

        File projects60File = new File("src/main/resources/sampleSize60Projects.xlsx");
        File projects120File = new File("src/main/resources/sampleSize120Projects.xlsx");
        File projects240File = new File("src/main/resources/sampleSize240Projects.xlsx");
        File projects500File = new File("src/main/resources/sampleSize500Projects.xlsx");

        File professors60File = new File("src/main/resources/sampleSizeProfessorsfor60Students.xlsx");
        File professors120File = new File("src/main/resources/sampleSizeProfessorsfor120Students.xlsx");
        File professors240File = new File("src/main/resources/sampleSizeProfessorsfor240Students.xlsx");
        File professors500File = new File("src/main/resources/sampleSizeProfessorsfor500Students.xlsx");


        data60Students = new Data(projects60File, students60File, professors60File);
		data120Students = new Data(projects120File, students120File, professors120File);
		data240Students = new Data(projects240File, students240File, professors240File);
		data500Students = new Data(projects500File, students500File, professors500File);

        
        sol60 = new Solution(data60Students);
        sol120 = new Solution(data120Students);
        sol240 = new Solution(data240Students);
        sol500 = new Solution(data500Students);

        solutions = new ArrayList<Solution>();
        solutions.add(sol60);
        solutions.add(sol120);
        solutions.add(sol240);
        solutions.add(sol500);


    }

    // Ensures all possible values return a temperature in the correct range(0 - 100).
    @Test
    public void testCalculateTemperature(){
        for(Solution solution : solutions ){
            for(int i = 0; i<101; i++){
                assertTrue(((solution.calculateTemperature(i/100))>=0) && ((solution.calculateTemperature(i))<=100),
                 "index " + i + " for size " + solution.getCount() + " Value calculated:" + solution.calculateTemperature(i));
            }
        }
    }

    //Ensure fitness is a positive value
    @Test
    public void testCalculateFitness(){
        for(Solution solution : solutions ){
            for(int i = 0; i<101; i++){
                assertTrue(((solution.calculateFitness(i/100))>=0) && ((solution.calculateTemperature(i))<=100),
                 "index " + i + " for size " + solution.getCount() + " Value calculated:" + solution.calculateFitness(i));
            }
        }
    }

    //Compare size and HashMaps of a Solution to a new Random change of it.
    @Test
    public void testRandomChange(){
        for(Solution solution : solutions){
            int oldCount = solution.getCount();
            Set<Project> oldProjects = solution.getSolution().keySet();
            List<Student> oldStudents = new ArrayList<Student>();

            for(Student student : solution.getSolution().values()){
                oldStudents.add(student);
            }
            
            solution.randomChange();
            int newCount = solution.getCount();
            Set<Project> newProjects = solution.getSolution().keySet();
            List<Student> newStudents = new ArrayList<Student>();

            for(Student student : solution.getSolution().values()){
                newStudents.add(student);
            }

            assertEquals(oldCount, newCount, "Size of solutions not equal");
            assertTrue(oldProjects.equals(newProjects), "Projects do not match");
            for(Student oldStudent : oldStudents){
                assertTrue(newStudents.contains(oldStudent), "Missing student(s) in new Solution");
            }
        }
    }

}