    package com.kovid.a1;

    import org.junit.After;
    import org.junit.Test;

    import static org.junit.jupiter.api.Assertions.assertEquals;

    import org.testfx.api.FxAssert;
    import org.testfx.framework.junit.ApplicationTest;
    import org.testfx.matcher.base.NodeMatchers;
    import org.testfx.matcher.base.ParentMatchers;

    import javafx.scene.Scene;
    import javafx.scene.control.Button;
    import javafx.stage.Stage;

    public class HomeSceneTest extends ApplicationTest {

        private Scene welcomeSceneTester;
        private Stage windowTest;

        @Override
        public void start(Stage Stage) throws Exception {
            // TODO Auto-generated method stub

            // Scene testScene = A1Application.homeSceneTester();
            windowTest = Stage;
            windowTest.setTitle("Kovid fiche fiche");
            welcomeSceneTester = A1Application.homeSceneTester();
            windowTest.setScene(welcomeSceneTester);
            windowTest.show();
        }

        @Test
        public void testButtonsExist(){
            
            FxAssert.verifyThat(welcomeSceneTester.getRoot(), ParentMatchers.hasChildren(2));
            FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChild(".button"));
        }

        @Test
        public void testGAButtonPress(){
            Button ga = (Button) welcomeSceneTester.getRoot().lookup("#GA");
            assertEquals(ga.getText(), "Genetic Algorithm");
            clickOn(ga);
            assertEquals(ga.getText(), "Clicked");
        }

        @Test
        public void testSAButtonPress(){
            Button sa = (Button) welcomeSceneTester.getRoot().lookup("#SA");
            assertEquals(sa.getText(), "Simulated Annealing");
            clickOn(sa);
            assertEquals(sa.getText(), "Clicked");
        }

    }