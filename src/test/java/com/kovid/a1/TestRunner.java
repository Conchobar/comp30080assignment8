//Class that runs all the test classes
package com.kovid.a1;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {

    public static void main(String[] args){

        //Run all the tests in a particular class and print a successful message if all tests pass and vice-versa
        Result result = JUnitCore.runClasses(DataTests.class);
        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        if(result.wasSuccessful()){
            System.out.println("Data Tests finished successfully...");
        }

        result = JUnitCore.runClasses(GenerateSolutionTests.class);
        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        if(result.wasSuccessful()){
            System.out.println("GenerateSolution Tests finished successfully...");
        }

        result = JUnitCore.runClasses(SolutionTests.class);
        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        if(result.wasSuccessful()){
            System.out.println("Solution Tests finished successfully...");
        }

        result = JUnitCore.runClasses(SATestScene.class);
        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        if(result.wasSuccessful()){
            System.out.println("Simulated Annealing GUI Tests finished successfully...");
        }
        result = JUnitCore.runClasses(GASceneTest.class);
        for(Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }
        if(result.wasSuccessful()){
            System.out.println("Genetic Algorithm GUI Tests finished successfully...");
        }


    }
}