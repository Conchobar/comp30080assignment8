package com.kovid.a1;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateSolutionTests {
    Data data60Students, data120Students, data240Students, data500Students;
    GenerateSolution gs60, gs120, gs240, gs500;
    List<GenerateSolution> generatedSolutions;

    public GenerateSolutionTests() throws IOException{

        File students60File = new File("src/main/resources/sampleSize60Students.xlsx");
        File students120File = new File("src/main/resources/sampleSize120Students.xlsx");
        File students240File = new File("src/main/resources/sampleSize240Students.xlsx");
        File students500File = new File("src/main/resources/sampleSize500Students.xlsx");

        File projects60File = new File("src/main/resources/sampleSize60Projects.xlsx");
        File projects120File = new File("src/main/resources/sampleSize120Projects.xlsx");
        File projects240File = new File("src/main/resources/sampleSize240Projects.xlsx");
        File projects500File = new File("src/main/resources/sampleSize500Projects.xlsx");

        File professors60File = new File("src/main/resources/sampleSizeProfessorsfor60Students.xlsx");
        File professors120File = new File("src/main/resources/sampleSizeProfessorsfor120Students.xlsx");
        File professors240File = new File("src/main/resources/sampleSizeProfessorsfor240Students.xlsx");
        File professors500File = new File("src/main/resources/sampleSizeProfessorsfor500Students.xlsx");


        data60Students = new Data(projects60File, students60File, professors60File);
		data120Students = new Data(projects120File, students120File, professors120File);
		data240Students = new Data(projects240File, students240File, professors240File);
		data500Students = new Data(projects500File, students500File, professors500File);


		gs60 = new GenerateSolution(data60Students.getProjects(), data60Students.getStudents());
		gs120 = new GenerateSolution(data120Students.getProjects(), data120Students.getStudents());
		gs240 = new GenerateSolution(data240Students.getProjects(), data240Students.getStudents());
		gs500 = new GenerateSolution(data500Students.getProjects(), data500Students.getStudents());

        generatedSolutions = new ArrayList<GenerateSolution>();
        generatedSolutions.add(gs60);
        generatedSolutions.add(gs120);
        generatedSolutions.add(gs240);
        generatedSolutions.add(gs500);

        
    }

    @Test
    public void testGenerateSolution(){
        for(GenerateSolution gs : generatedSolutions){
                    assertEquals(gs.getAllocatedProjects().size(), gs.getStudents().size(), "Allocated Projects and Students size mismatch.");
                    assertEquals(gs.getAllocatedProjects().stream().distinct().count(), gs.getAllocatedProjects().size(), "Allocated Projects List contains duplicate values.");
                    assertEquals(gs.getStudents().stream().distinct().count(), gs.getStudents().size(), "Students List contains duplicate values.");
        }
    }
    
}