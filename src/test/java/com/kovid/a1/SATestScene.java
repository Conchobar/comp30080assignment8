package com.kovid.a1;

import org.junit.After;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.api.FxRobotContext;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;
import org.testfx.matcher.base.ParentMatchers;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class SATestScene extends ApplicationTest{
    private Scene welcomeSceneTester;
    private Stage windowTest;

    @Override
    public void start(Stage Stage) throws Exception {
        // TODO Auto-generated method stub

        // Scene testScene = A1Application.homeSceneTester();
        windowTest = Stage;
        windowTest.setTitle("Genetic Algorithm Test");
        welcomeSceneTester = SASceneTestFrame.simulatedAnnealingScene();
        windowTest.setScene(welcomeSceneTester);
        windowTest.show();
    }

    @Test
    public void testComponentsPresent() throws InterruptedException {
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChild(".button"));
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChild(".slider"));
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChild(".label"));
        assertEquals(welcomeSceneTester.getRoot().getChildrenUnmodifiable().size(), 3);
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChildren(10, ".button"));
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChildren(1, ".slider"));
        FxAssert.verifyThat(welcomeSceneTester.getRoot(), NodeMatchers.hasChildren(10, ".label"));
    }

    @Test
    public void testBack() {
        Button back = (Button) welcomeSceneTester.getRoot().lookup("#back");

        assertEquals("Back", back.getText());
        clickOn(back);
        assertEquals("Clicked", back.getText());
    }

    @Test
    public void testRunIsDisabled() {
        Button run = (Button) welcomeSceneTester.getRoot().lookup("#run");
        assertTrue((run.isDisabled()));
        assertEquals("Run", run.getText());
        clickOn(run);
        sleep(2000, TimeUnit.MILLISECONDS);
        assertEquals("Run", run.getText());
    }

    @Test
    public void sliderTests() {
        Slider gpa = (Slider) welcomeSceneTester.getRoot().lookup("#gpaSlider");
        assertTrue(gpa.isVisible());
        assertTrue(gpa.isShowTickLabels());
        assertTrue(gpa.isShowTickMarks());
    }

    @Test
    public void testStudentFileButton() {
        Button studentFile = (Button) welcomeSceneTester.getRoot().lookup("#studentsFileButton");
        assertTrue(studentFile.isVisible());
        assertTrue(!(studentFile.isDisabled()));
        clickOn(studentFile);
        sleep(1000, TimeUnit.MILLISECONDS);
        clickOn(studentFile);

        
    }

    @Test
    public void testProjectFileButton() {
        Button projectFile = (Button) welcomeSceneTester.getRoot().lookup("#projectsFileButton");
        Button studentFile = (Button) welcomeSceneTester.getRoot().lookup("#studentsFileButton");

        assertTrue(projectFile.isVisible());
        assertTrue(!(projectFile.isDisabled()));
        clickOn(projectFile);
        sleep(1000, TimeUnit.MILLISECONDS);
        clickOn(studentFile);

    }

    @Test
    public void testProfesssorFileButton() throws TimeoutException {
        Button professorsFile = (Button) welcomeSceneTester.getRoot().lookup("#professorsFileButton");
        Button studentFile = (Button) welcomeSceneTester.getRoot().lookup("#studentsFileButton");

        assertTrue(professorsFile.isVisible());
        assertTrue(!(professorsFile.isDisabled()));
        clickOn(professorsFile);        
        sleep(1000, TimeUnit.MILLISECONDS);
        clickOn(studentFile);

    }

    @Test
    public void testSingleFileButton() throws Exception {
        Button singleFile = (Button) welcomeSceneTester.getRoot().lookup("#singleFileButton");
        Button studentFile = (Button) welcomeSceneTester.getRoot().lookup("#studentsFileButton");

        assertTrue(singleFile.isVisible());
        assertTrue(!(singleFile.isDisabled()));
        clickOn(singleFile);
        sleep(1000, TimeUnit.MILLISECONDS);
        clickOn(studentFile);
    }


}