package com.kovid.a1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest

public class DataTests {
    Data data60Students, data120Students, data240Students, data500Students;
    List<Data> dataSets;

    public DataTests() throws IOException{

        File students60File = new File("src/main/resources/sampleSize60Students.xlsx");
        File students120File = new File("src/main/resources/sampleSize120Students.xlsx");
        File students240File = new File("src/main/resources/sampleSize240Students.xlsx");
        File students500File = new File("src/main/resources/sampleSize500Students.xlsx");

        File projects60File = new File("src/main/resources/sampleSize60Projects.xlsx");
        File projects120File = new File("src/main/resources/sampleSize120Projects.xlsx");
        File projects240File = new File("src/main/resources/sampleSize240Projects.xlsx");
        File projects500File = new File("src/main/resources/sampleSize500Projects.xlsx");

        File professors60File = new File("src/main/resources/sampleSizeProfessorsfor60Students.xlsx");
        File professors120File = new File("src/main/resources/sampleSizeProfessorsfor120Students.xlsx");
        File professors240File = new File("src/main/resources/sampleSizeProfessorsfor240Students.xlsx");
        File professors500File = new File("src/main/resources/sampleSizeProfessorsfor500Students.xlsx");


        data60Students = new Data(projects60File, students60File, professors60File);
		data120Students = new Data(projects120File, students120File, professors120File);
		data240Students = new Data(projects240File, students240File, professors240File);
		data500Students = new Data(projects500File, students500File, professors500File);

        dataSets = new ArrayList<Data>();
        dataSets.add(data60Students);
        dataSets.add(data120Students);
        dataSets.add(data240Students);
        dataSets.add(data500Students);

    }


	@Test
	public void testSizeOfDatasetStudents(){
		assertEquals(60, data60Students.getStudents().size(), "60");
		assertEquals(120, data120Students.getStudents().size(), "120");
		assertEquals(240, data240Students.getStudents().size(), "240");
		assertEquals(500, data500Students.getStudents().size(), "500");
	}

    //Ensures that the professor list generated is within the acceptable range
	@Test
	public void testSizeOfDatasetProfessors(){
        for(Data data : dataSets){
            assertTrue((data.getProfessors().size()<((data.getStudents().size()*.6))) && (data.getProfessors().size() > (data.getStudents().size()*.4)), 
            "Error in " + data.getStudents().size() + " students dataset.");
        }
    }
    
    //Ensures project list within acceptable range
    @Test
    public void testSizeOfDatasetProjects(){
        for(Data data : dataSets){
            assertTrue((data.getProjects().size()<((data.getStudents().size()*1.75))) && (data.getProjects().size() > (data.getStudents().size()*1.2)), 
            "Error in " + data.getStudents().size() + " students dataset.");
        }
    }

    @Test
    public void testNoNullFieldsStudents(){
        for(Data data: dataSets){
            for(Student student : data.getStudents()){
                assertTrue( ((student.getFirstName()!=null) && !student.getFirstName().isEmpty())
                         && ((student.getSurname()!=null) && !student.getSurname().isEmpty())
                         && ((student.getStream()!=null) && !student.getStream().isEmpty())
                         && ((student.getStream()!=null) && !student.getStream().isEmpty())
                         && ((student.getPreferenceList()!=null) && !student.getPreferenceList().isEmpty())
                         ,student.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void testStudentGPARange(){
        for(Data data : dataSets){
            for(Student student : data.getStudents()){
                assertTrue((student.getGpa()>=2.0) && (student.getGpa()<=4.2),
                student.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void testStudentPreferenceListSize(){
        for(Data data: dataSets){
            for(Student student : data.getStudents()){
                assertTrue(student.getPreferenceList().size()==10,
                student.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void validStudentStream(){
        for(Data data : dataSets){
            for(Student student : data.getStudents()){
                assertTrue(student.getStream().equals("DS") || student.getStream().equals("CS"),
                student.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void testNoNullProjects(){
        for(Data data : dataSets){
            for(Project project : data.getProjects()){
                assertTrue( (project.getTitle()!=null && !project.getTitle().isEmpty())
                && (project.getProfessor()!=null && !project.getProfessor().isEmpty())
                && (project.getProjectStream()!=null && !project.getProjectStream().isEmpty())
                   ,project.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void validProjectStream(){
        for(Data data : dataSets){
            for(Project project : data.getProjects()){
                assertTrue( (project.getProjectStream().equals("CS"))
                         || (project.getProjectStream().equals("DS"))
                         || (project.getProjectStream().equals("CS + DS")) 
                         ,project.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

    @Test
    public void testNoNullProfessors(){
        for(Data data : dataSets){
            for(Professor professor : data.getProfessors()){
                assertTrue( (professor.getInterest()!=null && !professor.getInterest().isEmpty())
                          &&(professor.getName()!=null && !professor.getName().isEmpty())
                          &&(!professor.getProjects().isEmpty())
                          ,professor.toString() + " @ " + data.getStudents().size() + " students dataset.");
            }
        }
    }

}