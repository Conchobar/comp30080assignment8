package com.kovid.a1;

public interface Measurable {

    public double calculateTemperature(double weight);

    public double calculateFitness(double weight);

}