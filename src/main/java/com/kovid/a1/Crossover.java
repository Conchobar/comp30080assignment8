package com.kovid.a1;
import java.util.*;

//Class that performs the crossover between 2 parents 
public class Crossover {

    private Solution parent1, parent2, offSpring;
    private double splitWeight;
    private int sampleSizeChoice;

    Crossover(Solution parent1, Solution parent2, double splitWeight, int sampleSizeChoice){
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.splitWeight = splitWeight;
        this.sampleSizeChoice = sampleSizeChoice;
        this.offSpring = doCrossover();
    }

    //Method that creates an offspring by crossing over two parents
    private Solution doCrossover(){
        Solution offspring = new Solution();
        HashMap<Student, Project> parent1Solution = new HashMap<Student, Project>();
        HashMap<Student, Project> parent2Solution = new HashMap<Student, Project>();
        List<Student> students = new ArrayList<Student>();

         for(Map.Entry<Project, Student> entry : parent1.getSolutionData().entrySet()){
             parent1Solution.put(entry.getValue(), entry.getKey());
        }

         for(Map.Entry<Project, Student> entry : parent2.getSolutionData().entrySet()){
             parent2Solution.put(entry.getValue(), entry.getKey());
         }

         //Create a student list to compare p1 and p2 hashmap keys
         for(Student student : parent1Solution.keySet()){
             students.add(student);
         }

         if(parent1Solution.keySet().size()<sampleSizeChoice){
             System.out.println("SIZE OF PARENT IS PROBLEM. Size: " + parent1Solution.keySet().size());
         }

         if(parent2Solution.keySet().size()<sampleSizeChoice){
            System.out.println("SIZE OF PARENT IS PROBLEM. Size " + parent2Solution.keySet().size());
        }

         HashMap<Student, Project> combinedList = new HashMap<Student, Project>();

         //Depending on the split varibale we make our offspring by taking a fit percentage of students from each aprent 
         int i = 0;
         for(Student student : students){
            int fractionp1 = (int)Math.floor(students.size()*splitWeight);
            if(i<fractionp1){
                for(Student candidate : parent1Solution.keySet()){
                    if(student.getFirstName().equals(candidate.getFirstName())
                    && (student.getSurname().equals(candidate.getSurname())) && student.getStudentNumber()==candidate.getStudentNumber()){
                        combinedList.put(candidate, parent1Solution.get(candidate));
                    }
                }
            }
            else{
                for(Student candidate : parent2Solution.keySet()){
                    if(student.getFirstName().equals(candidate.getFirstName())
                    && (student.getSurname().equals(candidate.getSurname())) && student.getStudentNumber()==candidate.getStudentNumber()){
                        combinedList.put(candidate, parent2Solution.get(candidate));
                    }
                }
            }
            i++;
        }

         //HashMap that will have a pair of students as key and value who have the same project assigned to them in comobinedList
         HashMap<Student, Student> studentsWithSameProject = new HashMap<Student, Student>();

         //List where we add projects that have been assigned to a student in combinedList
         List<Project> projectsAssignedAlready = new ArrayList<Project>();

         for(Map.Entry<Student, Project> entry : combinedList.entrySet()){
             Student st = entry.getKey();
             Project pr = entry.getValue();
             boolean bool = true;
             for(Project proj: projectsAssignedAlready){
                 if(proj.getTitle().equals(pr.getTitle()) && proj.getProfessor().equals(pr.getProfessor())){
                     bool = false;
                     break;
                 }
             }
             //If project hasn't been assigned to anybody, add it to the list
             if(bool){
                 projectsAssignedAlready.add(pr);
             }

             //If project is assigned already, find the student who it was aassigned to and add both students to the new HashMap
             else{
                for(Map.Entry<Student, Project> entry2 : combinedList.entrySet()){
                    Student stu = entry2.getKey();
                    Project pro = entry2.getValue();
                    if(pro.getTitle().equals(pr.getTitle()) && pro.getProfessor().equals(pr.getProfessor())){
                        studentsWithSameProject.put(stu, st);
                        break;
                    }
                }
             }
         }

         //Go through the pairs of students who have the same project
         //Get the projects they were assigned in Parent1 and Parent2
         //If their exists a project that has not been assigned to any other student in combinedList, we update that students project
         //If all projects are already assigned, then crossover isnt possible
         for(Map.Entry<Student, Student> entryStudents : studentsWithSameProject.entrySet()){
            Student student1 = entryStudents.getKey();
            Student student2 = entryStudents.getValue();
            Project project1inParent1 = new Project();
            Project project1inParent2 = new Project();
            for(Map.Entry<Student, Project> entry : parent1Solution.entrySet()){
                Student stud = entry.getKey();
                Project currPro = entry.getValue();
                if(student1.getStudentNumber()==stud.getStudentNumber()){
                    project1inParent1 = currPro;
                }
           }

            for(Map.Entry<Student, Project> entry : parent2Solution.entrySet()){
                Student stud = entry.getKey();
                Project currPro = entry.getValue();
                if(student1.getStudentNumber()==stud.getStudentNumber()){
                project1inParent2 = currPro;
                }
            }

            Project project2inParent1  = new Project();
            Project project2inParent2  = new Project();
            for(Map.Entry<Student, Project> entry : parent1Solution.entrySet()){
                Student stud = entry.getKey();
                Project currPro = entry.getValue();
                if(student2.getStudentNumber()==stud.getStudentNumber()){
                    project2inParent1 = currPro;
                }
           }

            for(Map.Entry<Student, Project> entry : parent2Solution.entrySet()){
                Student stud = entry.getKey();
                Project currPro = entry.getValue();
                if(student2.getStudentNumber()==stud.getStudentNumber()){
                project2inParent2 = currPro;
                }
            }

            Boolean project1inParent1bool = true;
            Boolean project1inParent2bool = true;

            Boolean project2inParent1bool = true;
            Boolean project2inParent2bool = true;

            //If project is aleady asigned set the boolean to indicate that
            for(Project projectsAssigned : projectsAssignedAlready){
                if(project1inParent1.getTitle().equals(projectsAssigned.getTitle()) && project1inParent1.getProfessor().equals(projectsAssigned.getProfessor())){
                    project1inParent1bool = false;
                }
                if(project1inParent2.getTitle().equals(projectsAssigned.getTitle()) && project1inParent2.getProfessor().equals(projectsAssigned.getProfessor())){
                    project1inParent2bool = false;
                }
                if(project2inParent1.getTitle().equals(projectsAssigned.getTitle()) && project2inParent1.getProfessor().equals(projectsAssigned.getProfessor())){
                    project2inParent1bool = false;
                }
                if(project2inParent2.getTitle().equals(projectsAssigned.getTitle()) && project1inParent2.getProfessor().equals(projectsAssigned.getProfessor())){
                    project2inParent2bool = false;
                }
            }
            //If all projects are assigned already, this crossover isnt valid, we move to the next one
            if(!project1inParent1bool && !project1inParent2bool && !project2inParent1bool && !project2inParent2bool){
                offspring.setIsValidOffspring(false);
                break;
                //Crossover isnt possible
            }
            //If we find a project that hasn't been assigned to any student, we swap the project for that student with the current project
            else if(project1inParent1bool){
                combinedList.put(student1, project1inParent1);
            }
            else if(project2inParent1bool){
                combinedList.put(student2, project2inParent1);
            }
            else if(project1inParent2bool){
                combinedList.put(student1, project1inParent2);
            }
            else if(project2inParent2bool){
                combinedList.put(student2, project2inParent2);
            }
         }

         HashMap<Project, Student> combinedCorrected = new HashMap<Project, Student>();

         for(Map.Entry<Student, Project> entry : combinedList.entrySet()){
             combinedCorrected.put(entry.getValue(), entry.getKey());
         }

         List<Student> combinedStudents = new ArrayList<Student>();
         List<Project> combinedProjects = new ArrayList<Project>();

         for(Map.Entry<Project, Student> entry : combinedCorrected.entrySet()){
             combinedStudents.add(entry.getValue());
             combinedProjects.add(entry.getKey());
         }

         //We set the instance varibales for the offspring and return it
         offspring.setStudents(combinedStudents);
         offspring.setAllocatedProjects(combinedProjects);
         offspring.setSolutionData(combinedCorrected);
         if(offspring.getSolutionData().size()<sampleSizeChoice){
             offspring.setIsValidOffspring(false);
         }
        return offspring;
         
    }

    public Solution getOffSpring(){
        return this.offSpring;
    }
}