package com.kovid.a1;
import java.io.IOException;
import java.text.ParseException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SAScene extends FileIO{
    private String dataSetChoice = "0", acceptanceGroupChoice, coolingGroupChoice ;
    private double gpaWeight = 50;
    private Stage window;
    private Scene welcomeScene;
    private Button writeSa, showSolution, showSolutionBeforeSA, help;
    private File studentsFile, projectsFile, professorsFile, solutionFile, singleFile;
    private VBox output;
    private VBox running;
    private Data data;
    private boolean threeFilesChoice = false, file1=false, file2=false, file3=false;
    private boolean singleFileChoice = false;
    private String edits;
    
    //Parametrized constructor to set the stage and welcome scene
    SAScene(Stage window, Scene welcomeScene){
        this.window = window;
        this.welcomeScene = welcomeScene;
    }

    //Method that creates the SA Scene and returns it to be displayed on the GUI
    public Scene simulatedAnnealingScene(){

        //Create a splitpane container that has the GUI on one side and the output on the other
        SplitPane splitPane = new SplitPane();
        TextFlow tf = new TextFlow();
        Button run = new Button("Run");
        run.setDisable(true);
        tf.setPrefHeight(1280);
        tf.setMaxWidth(1600);
        tf.setPrefWidth(1200);
        tf.setBackground(new Background(new BackgroundFill(Color.rgb(255, 255, 255), null, null)));
        
        ScrollPane scrollPane = new ScrollPane(tf);
        scrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);

        output = new VBox(new Label("Output"), scrollPane);

        //File and Directory select options
        //Buttons to choose individual files to run programme with.
        //Button that uses the FileChooser object to import the students file to the application
        Button studentFileButton = new Button("Add Students File");
        Label studentFileLabel = new Label("No File Selected");
        HBox studentFileInfo = new HBox(studentFileButton, studentFileLabel);
        studentFileButton.setOnAction(value -> {
            String message = "Correct";
            FileChooser studentsChooser = new FileChooser();
            this.setStudentsFile(studentsChooser.showOpenDialog(window));
            studentFileLabel.setText(this.getStudentsFile().getName());
            if((!(projectsFile == null)&&(!(professorsFile == null)))){
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }
            }
            if(!(message.equals("Correct"))){
                final Stage myDialog = new Stage();
                myDialog.setTitle("Student File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);

            }
            else if((message.equals("Correct"))&&(!(projectsFile == null))&&(!(professorsFile == null))){
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;

            }
        });

        Button projectsFileButton = new Button("Add Projects File");
        Label projectsFileLabel = new Label("No File Selected");
        HBox projectsFileInfo = new HBox(projectsFileButton, projectsFileLabel);
        projectsFileButton.setOnAction(value -> {
            FileChooser projectsChooser = new FileChooser();
            this.setProjectsFile(projectsChooser.showOpenDialog(window));
            projectsFileLabel.setText(this.getProjectsFile().getName());
            int index = this.getProjectsFile().getName().indexOf('P');
            dataSetChoice = this.getProjectsFile().getName().substring(10, index);
            System.out.println("substring works : " + dataSetChoice);
            String message = "Correct";
            if((!(studentsFile == null)&&(!(professorsFile == null)))){
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if(!(message.equals("Correct"))){
                final Stage myDialog = new Stage();
                myDialog.setTitle("Project File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);

            }
            else if((message.equals("Correct"))&&(!(studentsFile == null))&&(!(professorsFile == null))){
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;

            }
        });

        // Button that uses the FileChooser object to import the professors file to the
        // application
        Button professorsFileButton = new Button("Add Professors File");
        Label professorsFileLabel = new Label("No File Selected");
        HBox professorFileInfo = new HBox(professorsFileButton, professorsFileLabel);
        professorsFileButton.setOnAction(value -> {
            String message = "Correct";
            FileChooser proffesorsChooser = new FileChooser();
            this.setProfessorsFile(proffesorsChooser.showOpenDialog(window));
            professorsFileLabel.setText(this.getProfessorsFile().getName());
            if((!(projectsFile == null)&&(!(studentsFile == null)))){
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if(!(message.equals("Correct"))){
                final Stage myDialog = new Stage();
                myDialog.setTitle("Professor File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);
            }
            else if((message.equals("Correct"))&&(!(studentsFile == null))&&(!(projectsFile == null))){
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;
            }
        });

        Text t = new Text();
        t.setText("\t    OR");
        t.setFont(Font.font("Verdana", FontWeight.BOLD, 30));
        t.setFill(Color.BLUE); 
        
        Button singleFileButton = new Button("Add Single File");
        Label singleFileLabel = new Label("No File Selected");
        HBox singleFileBox = new HBox(singleFileButton, singleFileLabel);

        singleFileButton.setOnAction(value ->{
            FileChooser singleFileChooser = new FileChooser();
            this.setSingleFile(singleFileChooser.showOpenDialog(window));
            singleFileLabel.setText(this.getSingleFile().getName());
            threeFilesChoice = false;
            singleFileChoice = true;
      
            //Validate file
            String message = "";
            
            try {
                message = FileIO.isFileValid(singleFile);
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            if(!(message.equals("Correct"))){
                final Stage myDialog = new Stage();
                myDialog.setTitle("Single File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);
            }
            else{
                run.setDisable(false);
                singleFileChoice = true;
                threeFilesChoice = false;
            }
        });



        //Radio button to select the cooling function user wants to use.
        //Basic Cooling slected by default
        Label coolingScheduleLabel = new Label("Select Cooling Function:");
        RadioButton fastCooling = new RadioButton("Fast Cooling");
        RadioButton slowCooling = new RadioButton("Slow Cooling");
        RadioButton decreasingRateCooling = new RadioButton("Decreasing Rate");
        RadioButton increasingRateCooling = new RadioButton("Increasing Rate");
        HBox coolingScheduleSelect = new HBox( fastCooling, slowCooling, decreasingRateCooling, increasingRateCooling);
        ToggleGroup coolingGroup = new ToggleGroup();    
        fastCooling.setToggleGroup(coolingGroup);
        slowCooling.setToggleGroup(coolingGroup);
        decreasingRateCooling.setToggleGroup(coolingGroup);
        increasingRateCooling.setToggleGroup(coolingGroup);
        decreasingRateCooling.setSelected(true);
        coolingGroupChoice = decreasingRateCooling.getText();

        //Listener that keeps track of changes to cooling selection
        coolingGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1){
                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle();
                    coolingGroupChoice = chk.getText();
             // Cast object to radio button
            System.out.println("Selected Radio Button - "+chk.getText());
            }
        });

        //Radio button to select the acceptance function user wants to use.
        //Basic Acceptance slected by default
        Label acceptanceFunctionLabel = new Label("Select Acceptance Function:");
        RadioButton basicAcceptance = new RadioButton("Basic");
        RadioButton boltzmannAcceptance = new RadioButton("Boltzmann");
        RadioButton mockAcceptance = new RadioButton("Mock");
        HBox acceptanceFunctionSelect = new HBox(basicAcceptance, boltzmannAcceptance, mockAcceptance);
        ToggleGroup acceptanceGroup = new ToggleGroup();
        basicAcceptance.setToggleGroup(acceptanceGroup);
        boltzmannAcceptance.setToggleGroup(acceptanceGroup);
        mockAcceptance.setToggleGroup(acceptanceGroup);
        mockAcceptance.setSelected(true);
        acceptanceGroupChoice = mockAcceptance.getText();

        //Listener to keep track of chnages made to the acceptance selection
        acceptanceGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1){
                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle();
                acceptanceGroupChoice = chk.getText();
                System.out.println("Selected Radio Button - " + chk.getText());
            }
        });

        //Slider to set GPA percentage, set to 50 by default
        Label gpaLabel = new Label("GPA Percentage");
        Slider gpaSlider = new Slider(0, 100, 50);
        gpaSlider.setMajorTickUnit(10.0);
        gpaSlider.setMinorTickCount(10);
        gpaSlider.setSnapToTicks(true);
        gpaSlider.setShowTickMarks(true);
        gpaSlider.setShowTickLabels(true);

        //Listener to keep track of changes made to GPA percentage
        gpaSlider.valueProperty().addListener(new ChangeListener<Number>(){
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val){
                gpaWeight = new_val.doubleValue()/100;
            }
        });

        Label hillLabel = new Label("Running Simulated Annealing");
        ProgressBar hillClimbingBar = new ProgressBar();
        hillClimbingBar.setPrefWidth(400);
        running = new VBox(hillLabel, hillClimbingBar);
        running.setVisible(false);

        showSolution = new Button("Display Solution After SA");
        showSolutionBeforeSA = new Button("Display Solution Before SA");
        //Decides how to construct the dataset before execution
        //When the user clicks the run button, we check for validity of files selected and give an error message if faulty data found
        //If data is okay, we run Simulated annealing
        run.setOnAction(value ->  {
            try {
                if(file1 && file2 && file3){
                    threeFilesChoice = true;
                    singleFileChoice = false;
                }
                boolean bool = true;
                //CAN DELETE HERE, FOR RADIO BUTTONS!!
                    if(!threeFilesChoice && !singleFileChoice){
                        final Stage myDialog = new Stage();
                        myDialog.initModality(Modality.WINDOW_MODAL);
                        Button okButton = new Button("CLOSE");
                        okButton.setOnAction(new EventHandler<ActionEvent>(){
                            public void handle(ActionEvent arg0) {
                                myDialog.close();
                            }
                        });             
                        HBox welcomeBox = new HBox();
                        welcomeBox.getChildren().add(new Label("Invalid data files, select files again"));
                        Scene myDialogScene = new Scene(welcomeBox,250,50);
                        myDialog.setScene(myDialogScene);
                        myDialog.show();
                    }
                    else{
                        System.out.println("before data obj created grand");
                        if(threeFilesChoice){
                            data = new Data(projectsFile, studentsFile, professorsFile);
                        }
                        else if(singleFileChoice){
                            data = new Data(singleFile);
                        }
                        bool = isDataValid(data);
                        if(bool){
                            final Stage myDialog = new Stage();
                            myDialog.initModality(Modality.WINDOW_MODAL);
                            Button okButton = new Button("CLOSE");
                            okButton.setOnAction(new EventHandler<ActionEvent>(){
                                public void handle(ActionEvent arg0) {
                                    myDialog.close();
                                }
                            });             
                            HBox welcomeBox = new HBox();
                            welcomeBox.getChildren().add(new Label("Invalid data files, select files again"));
                            Scene myDialogScene = new Scene(welcomeBox,250,50);
                            myDialog.setScene(myDialogScene);
                            myDialog.show();
                        }
                        else{
                            Runnable task = new Runnable(){
                                @Override
                                public void run() {
                                    try {
                                        runSimulatedAnnealing(data, output);                                    
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };
                            Thread backgroundThread = new Thread(task);
                            backgroundThread.setDaemon(true);
                            backgroundThread.start();
                            }    
                        }
            } catch (IOException e) {
                   e.printStackTrace();
               }
        });
    
         //Button to save solution to a file
         writeSa = new Button("Write to File");
         writeSa.setDisable(true);
         showSolution.setDisable(true);
         showSolutionBeforeSA.setDisable(true);
         Button back = new Button("Back");
         Button info = new Button("Info");
         info.setOnAction(value -> {
             System.out.println("Acceptance Function: " + acceptanceGroupChoice + " Cooling Schedule: " + coolingGroupChoice + " GPAWeight: " + gpaWeight);
         });

         help = new Button("Help");
         help.setOnAction(value -> {
             final Stage myDialog = new Stage();
             myDialog.setTitle("Help for Simulated Annealing");
             myDialog.initModality(Modality.WINDOW_MODAL);
             Button okButton = new Button("CLOSE");
             okButton.setOnAction(new EventHandler<ActionEvent>() {
                 public void handle(ActionEvent arg0) {
                     myDialog.close();
                 }
             });
 
             InputStream is;
             is = (this.getClass().getResourceAsStream("/SAInfo.txt"));
             BufferedReader br = new BufferedReader(new InputStreamReader(is));
             StringBuilder sb = new StringBuilder();
             String line;
             try {
                 while ((line = br.readLine()) != null) {
                     sb.append(line);
                     sb.append('\n');
                 }
             } catch (IOException e) {
                 // TODO Auto-generated catch block
                 e.printStackTrace();
             }
             String information = sb.toString();
             Text infoText = new Text(information);
             TextFlow text = new TextFlow();
             text.getChildren().add(infoText);
             Scene scene = new Scene(text);
             myDialog.setScene(scene);
             myDialog.setHeight(600);
             myDialog.setWidth(600);
             myDialog.show();
         });
        
        VBox saBox = new VBox(studentFileInfo, projectsFileInfo, professorFileInfo,t, singleFileBox, coolingScheduleLabel, coolingScheduleSelect,acceptanceFunctionLabel, acceptanceFunctionSelect,
                                 gpaLabel, gpaSlider, run, running,showSolutionBeforeSA, showSolution, writeSa, back, help);
       
        splitPane.getItems().addAll(saBox, output);
        Scene simulatedAnnealingScene = new Scene(splitPane,1280,720);
        back.setOnAction(value ->  {
            window.setScene(welcomeScene);
            window.setTitle("Home");
            window.show();
        });

        return simulatedAnnealingScene;
    }

    //Function that creates the simulated annealing object and returns the solution
    private void runSimulatedAnnealing(Data data, VBox output) throws IOException{
        writeSa.setDisable(true);
        ScrollPane sp = (ScrollPane)output.getChildren().get(1);
        TextFlow tf = (TextFlow) sp.getContent();

        String outputString = "";
        edits = data.studentEdits();

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(3);
        System.out.println("GPA Weight: " + gpaWeight);
        
        gpaWeight/=100;
        Solution solution = new Solution(data);
        List<Student> students = data.getStudents();
        System.out.println("Number of students: " + students.size());
        System.out.println("Number of students given project: " + solution.getCount());
        displaySolutionBeforeSA(solution);

        //Function that calls the method that returns the temperature/energy of the solution at hand
        double temperature = solution.calculateTemperature(gpaWeight);
        System.out.println("Temperature of solution is " + temperature);
        System.out.println("Fitness of solution is " + solution.calculateFitness(gpaWeight));
    
        //Call the random Change method that induces small random changes in the datatset
        solution.randomChange();
        solution.numOfRandomChangesMade();

        //Method that prints the changes made to the datset after calling the randomChange method
        temperature = solution.calculateTemperature(gpaWeight);
        System.out.println("New Temperature of solution is " + temperature);
        System.out.println("Fitness of solution is " + solution.calculateFitness(gpaWeight));
        System.out.println("ENERGY BEFORE SA IS  " + df.format(solution.calculateTemperature(gpaWeight)));
        System.out.println("Solution before: ");
        if(!singleFileChoice){
        solution.preferencesAssigned();
        solution.averagePreference();
        }
        else{
            solution.preferencesAssignedForSingleFile();
            solution.averagePreferenceForSingleFile();
        }

        HashMap<Project, Student> baseSolution = new HashMap<Project, Student>(solution.getSolutionData());
        SimulatedAnnealing sa = new SimulatedAnnealing(singleFileChoice, edits, solution, acceptanceGroupChoice, coolingGroupChoice, gpaWeight);
        CoolingSchedule cooling = sa.getCoolingSchedule();
        AcceptanceFunction acceptance = sa.getAcceptanceFunction();
        double lowestEnergy = solution.calculateTemperature(gpaWeight);
        Solution bestSolution = new Solution();
        String sstr="\nBefore SA:\n";

        //Pre-execution screen print here
        if(!singleFileChoice){
        sstr+=solution.returnAveragePreference()+"\n";
        }
        else{
            sstr+=solution.returnAveragePreferenceForSingleFile()+"\n";
        }
        outputString += "\nEnergy Before SA: " + df.format(solution.calculateTemperature(gpaWeight));
        System.out.println("\nEnergy Before SA: " + df.format(solution.calculateTemperature(gpaWeight)));

        running.setVisible(true);
        sa.hillClimbing();
        running.setVisible(false);

        //post-execution ouput here

        System.out.println("Cooling Method: " + cooling.getName() + "\tAcceptance Function: " + acceptance.getName());
        System.out.println("Energy After SA: " + df.format(solution.calculateTemperature(gpaWeight)));
        outputString+= "\nEnergy After SA: " + df.format(solution.calculateTemperature(gpaWeight));
        outputString += "\n\n";
        sstr+="After SA:\n";
        if(!singleFileChoice){
        sstr+=solution.returnAveragePreference()+"\n";
        }
        else{
            sstr+=solution.returnAveragePreferenceForSingleFile()+"\n";
        }
        if(solution.calculateTemperature(gpaWeight)<lowestEnergy){
            lowestEnergy = solution.calculateTemperature(gpaWeight);
            bestSolution = solution;
        }
        if(!singleFileChoice){
            solution.preferencesAssigned();
            solution.averagePreference();
        }
        else{
            solution.preferencesAssignedForSingleFile();
            solution.averagePreferenceForSingleFile();
        }
        solution.setSolutionData(baseSolution);
        System.out.println("\nLowest Energy achieved is " + df.format(lowestEnergy) + ", using " + cooling.getName() + " and " + acceptance.getName());
        if(!singleFileChoice){
            bestSolution.preferencesAssigned();
            bestSolution.averagePreference();
        }
        else{
            bestSolution.preferencesAssignedForSingleFile();
            bestSolution.averagePreferenceForSingleFile();
        }
        if(singleFileChoice){
            List<Student> l = data.getStudentsWhoProposedTheirProject();
            for(Student s: l){
                Project p = s.getPreferenceList().get(0);
                bestSolution.getSolutionData().put(p, s);
            }
        }

        displaySolution(bestSolution);
        sstr += outputString;
        Text result = new Text(sstr);
        Platform.runLater(new Runnable(){

            @Override
            public void run() {
                tf.getChildren().add(result);
            }
        });
        edits+="\nStudents who got a project that was not in their preference list, due to giving an incomplete prefernece list are - \n";
        
        //Add the students who chose their own project back to the solution list
        for(Project p : bestSolution.getSolutionData().keySet()){
            Student s = bestSolution.getSolutionData().get(p);
            if(s.getOriginalNumOfProjects()<20){
                int index = s.getPreferenceList().indexOf(p);
                if(index>=s.getOriginalNumOfProjects()){
                    edits+=s.getFullName()+" gaove only "+s.getOriginalNumOfProjects()+" preferences"+"\n"; 
                }
            }
        }  
        printSolution(bestSolution, output, edits, singleFileChoice);
        writeSa.setDisable(false);
        FileChooser solutionChooser = new FileChooser();
        solutionChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLSX Files (*.xlsx)", "*.xlsx");
        solutionChooser.getExtensionFilters().add(extFilter);
        solutionChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
        //Use the FileChooser object to let user select the path where they want to save the solution
        writeSa.setOnAction(value -> {
            try {
                String path = System.getProperty("user.home") + File.separator + "SASolution";
                this.solutionFile = new File(path);
                sa.writeToFile(sa.getBestSolution());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            File destinationFile = solutionChooser.showSaveDialog(window);
            if(destinationFile!=null){
                try {
                    Files.copy(solutionFile.toPath(), destinationFile.toPath());
                    System.out.println("solution path is: " + solutionFile.getPath());
                    System.out.println("destination Path is: " + destinationFile.getPath());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
        });
    }

    //method that displays the bar chart on the screen
    private void displaySolutionBeforeSA(Solution sol){
        showSolutionBeforeSA.setDisable(false);
        int arr[];
        if(!singleFileChoice){
            arr = new int[10];
        }
        else{
            arr = new int[20];
        }
        int index=0;
        for(Project p : sol.getSolutionData().keySet()){
            Student s = sol.getSolutionData().get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())){
                    index = i;
                }
            }
            arr[index]++;
        }
        showSolutionBeforeSA.setOnAction(value -> {
            final Stage myDialog = new Stage();
            myDialog.setTitle("Solution Before Running SA");
            myDialog.initModality(Modality.WINDOW_MODAL);
            Button okButton = new Button("CLOSE");
            okButton.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent arg0) {
                    myDialog.close();
                }
            });             
            CategoryAxis xAxis    = new CategoryAxis();
            xAxis.setLabel("Prefrence Assigned");
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Students");
            BarChart barChart = new BarChart(xAxis, yAxis);
            XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.getData().add(new XYChart.Data("1st", arr[0]));
            dataSeries1.getData().add(new XYChart.Data("2nd", arr[1]));
            dataSeries1.getData().add(new XYChart.Data("3rd", arr[2]));
            dataSeries1.getData().add(new XYChart.Data("4th", arr[3]));
            dataSeries1.getData().add(new XYChart.Data("5th", arr[4]));
            dataSeries1.getData().add(new XYChart.Data("6th", arr[5]));
            dataSeries1.getData().add(new XYChart.Data("7th", arr[6]));
            dataSeries1.getData().add(new XYChart.Data("8th", arr[7]));
            dataSeries1.getData().add(new XYChart.Data("9th", arr[8]));
            dataSeries1.getData().add(new XYChart.Data("10th",arr[9]));
            if(singleFileChoice){
                dataSeries1.getData().add(new XYChart.Data("11th", arr[10]));
                dataSeries1.getData().add(new XYChart.Data("12th", arr[11]));
                dataSeries1.getData().add(new XYChart.Data("13th", arr[12]));
                dataSeries1.getData().add(new XYChart.Data("14th", arr[13]));
                dataSeries1.getData().add(new XYChart.Data("15th", arr[14]));
                dataSeries1.getData().add(new XYChart.Data("16th", arr[15]));
                dataSeries1.getData().add(new XYChart.Data("17th", arr[16]));
                dataSeries1.getData().add(new XYChart.Data("18th", arr[17]));
                dataSeries1.getData().add(new XYChart.Data("19th", arr[18]));
                dataSeries1.getData().add(new XYChart.Data("20th",arr[19]));
            }

            barChart.getData().add(dataSeries1);
            VBox vbox = new VBox(barChart);
            Scene scene = new Scene(vbox, 600, 600);
            myDialog.setScene(scene);
            myDialog.setHeight(600);
            myDialog.setWidth(600);
            myDialog.show();

        });
    }

    //Method that displays the solutin on teh screen
    private void displaySolution(Solution sol){
        showSolution.setDisable(false);
        int arr[];
        if(!singleFileChoice){
            arr = new int[10];
        }
        else{
            arr = new int[20];
        }
        int index=0;
        for(Project p : sol.getSolutionData().keySet()){
            Student s = sol.getSolutionData().get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())){
                    index = i;
                }
            }
            arr[index]++;
        }
        showSolution.setOnAction(value -> {
            final Stage myDialog = new Stage();
            myDialog.setTitle("Solution After Running SA");
            myDialog.initModality(Modality.WINDOW_MODAL);
            Button okButton = new Button("CLOSE");
            okButton.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent arg0) {
                    myDialog.close();
                }
            });             
            CategoryAxis xAxis    = new CategoryAxis();
            xAxis.setLabel("Prefrence Assigned");
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Students");
            BarChart barChart = new BarChart(xAxis, yAxis);
            XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.getData().add(new XYChart.Data("1st", arr[0]));
            dataSeries1.getData().add(new XYChart.Data("2nd", arr[1]));
            dataSeries1.getData().add(new XYChart.Data("3rd", arr[2]));
            dataSeries1.getData().add(new XYChart.Data("4th", arr[3]));
            dataSeries1.getData().add(new XYChart.Data("5th", arr[4]));
            dataSeries1.getData().add(new XYChart.Data("6th", arr[5]));
            dataSeries1.getData().add(new XYChart.Data("7th", arr[6]));
            dataSeries1.getData().add(new XYChart.Data("8th", arr[7]));
            dataSeries1.getData().add(new XYChart.Data("9th", arr[8]));
            dataSeries1.getData().add(new XYChart.Data("10th",arr[9]));
            if(singleFileChoice){
                dataSeries1.getData().add(new XYChart.Data("11th", arr[10]));
                dataSeries1.getData().add(new XYChart.Data("12th", arr[11]));
                dataSeries1.getData().add(new XYChart.Data("13th", arr[12]));
                dataSeries1.getData().add(new XYChart.Data("14th", arr[13]));
                dataSeries1.getData().add(new XYChart.Data("15th", arr[14]));
                dataSeries1.getData().add(new XYChart.Data("16th", arr[15]));
                dataSeries1.getData().add(new XYChart.Data("17th", arr[16]));
                dataSeries1.getData().add(new XYChart.Data("18th", arr[17]));
                dataSeries1.getData().add(new XYChart.Data("19th", arr[18]));
                dataSeries1.getData().add(new XYChart.Data("20th",arr[19]));
            }

            barChart.getData().add(dataSeries1);
            VBox vbox = new VBox(barChart);
            Scene scene = new Scene(vbox, 600, 600);
            myDialog.setScene(scene);
            myDialog.setHeight(600);
            myDialog.setWidth(600);
            myDialog.show();

        });
    }

    //Method that checks if the data is valid
    private boolean isDataValid(Data data){
        boolean bool = true;
        if(singleFileChoice){
            return false;
        }
        int sizeStudents = data.getStudents().size();
        int sizeProf = data.getProfessors().size();
        int sizeProj = data.getProjects().size();
        if(sizeStudents==2*sizeProf){
            if(sizeProj>0.8*sizeProf*3 && sizeProj<1.2*sizeProf*3){
                bool=false;
            }
        }
        return bool;
        
    }

    public File getStudentsFile() {
        return studentsFile;
    }

    public void setStudentsFile(File studentsFile) {
        this.studentsFile = studentsFile;
    }

    public File getProjectsFile() {
        return projectsFile;
    }

    public void setProjectsFile(File projectsFile) {
        this.projectsFile = projectsFile;
    }

    public File getProfessorsFile() {
        return professorsFile;
    }

    public void setProfessorsFile(File professorsFile) {
        this.professorsFile = professorsFile;
    }

    public File getSingleFile() {
        return singleFile;
    }

    public void setSingleFile(File singleFile) {
        this.singleFile = singleFile;
    }

}