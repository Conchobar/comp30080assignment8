package com.kovid.a1;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private int studentNumber;
    private String firstName="";
    private String surname="";
    private String fullName="";
    private String stream;
    private String proposer="";
    private Boolean isProposer=false;
    private double gpa;
    private List<Project> preferenceList = new ArrayList<Project>();
    private int tag, originalNumOfProjects=20;

    public Student(){

    }
    
    public Student(int studentNumber, String firstName, String surname, String stream){ 
        this.studentNumber = studentNumber;
        this.firstName = firstName;
        this.surname = surname;
        this.stream  = stream;
    }
    public Boolean getIsProposer(){
        return isProposer;
    }
    public void setIsProposer(boolean b){
        isProposer = b;
    }
    public String getProposer(){
        return proposer;
    }
    public void setProposer(String prop){
        proposer=prop;
    }
    public String getFullName(){
        return fullName;
    }
    public void setFullName(String name){
        fullName = name;
    }

    public String toString(){
        String str = "";
        str += "ID: " + this.studentNumber + "\t";
        str += "Name: " + this.fullName + "\t";
        if(isProposer){
            str+= "Proposer: Yes "+"\n"; 
        }
        else{
            str+= "Proposer: No "+"\n"; 
        }
        for(Project project : preferenceList){
            str += project.toString() + "\n";
        }

        return str;
    }

    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getName(){
        String str = firstName+" "+surname;
        return str;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public int getOriginalNumOfProjects(){
        return this.originalNumOfProjects;
    }

    public void setOriginalNumOfProjects(int num){
        this.originalNumOfProjects = num;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Project> getPreferenceList() {
        return preferenceList;
    }

    public void setPreferenceList(List<Project> preferenceList) {
        this.preferenceList = preferenceList;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }
}