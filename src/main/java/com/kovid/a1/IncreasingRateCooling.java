package com.kovid.a1;

public class IncreasingRateCooling implements CoolingSchedule{

    private double maxTemperature;
    private double temperature;
    private double coolingRate;
    private int iterations;
    private String name;

    //Default constructor
    public IncreasingRateCooling(){
        this.maxTemperature = 1000;
        this.temperature = maxTemperature;
        this.coolingRate = 1;
        this.iterations = 100;
        this.name = "Increasing Rate Cooling";

    }

    //Parameterized constructor that assigns values to instance variables
    public IncreasingRateCooling(double temperature, double coolingRate){
        this.maxTemperature = temperature;
        this.temperature = temperature;
        this.coolingRate = coolingRate;
        this.name = "Increasing Rate Cooling";

    }

    //Function that returns a boolean indicating when we should stop the program
    public Boolean stopOrNot(){
        if(temperature <= 0){
            temperature = maxTemperature;
            coolingRate = 1;
            return true;
        }
        else{
            return false;
        }
    }

    public void reduceTemperature(){
        temperature-=coolingRate;
        coolingRate+=(coolingRate/2);
        iterations=0;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getCoolingRate() {
        return coolingRate;
    }

    public void setCoolingRate(double coolingRate) {
        this.coolingRate = coolingRate;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}