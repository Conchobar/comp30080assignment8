package com.kovid.a1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Data{

    private List<Project> projects = new ArrayList<Project>();
    private List<Professor> professors = new ArrayList<Professor>();
    private List<Student> students= new ArrayList<Student>();
    private List<Integer> studentNums = new ArrayList<Integer>();
    private List<Project> projectsProposedByStudents = new ArrayList<Project>();
    private List<Student> studentsWhoProposedTheirProjects = new ArrayList<Student>();
    private List<Student> studentsWithAmendedPreferenceList = new ArrayList<Student>();
    private List<Student> studentsWithAmendedStudentNumber = new ArrayList<Student>();
    private int counterrr=1;
  
    //Parametrized constructor that calls the methods with the file as an argument
    public Data(File oneFile){
        readProjectsFromOneFile(oneFile);
        readStudentsFromOneFile(oneFile);
        fixData();
    }

    //Parametrized constructor that calls the methods with the file as an argument
    public Data(File projectsFile, File studentsFile, File professorsFile) throws IOException {
        this.projects = readExcelProjects(projectsFile);
        this.students = readExcelStudents(studentsFile);
        this.professors = readExcelProfessors(professorsFile);
    }

    //Method that reads the projects from the Single file
    private void readProjectsFromOneFile(File oneFile){
        InputStream fis;
    
        //Try catch block that exits the program with an error message if we can't read the file properly
        try {
            fis = new FileInputStream(oneFile);          
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                throw new ParseException("No sheet found on index 0", 1);
            }
        Iterator<Row> rowIterator = mySheet.iterator();
        boolean headSkip=true;
        int proposerCell=-1; 
        int cellCount=0;
        // Traversing over each row of XLSX file
            while(rowIterator.hasNext()){
                Row row = rowIterator.next();
                if(checkIfRowEmpty(row)){
                    continue;
                }
                //If statement that skips the first row of the file which is the title row
                if(headSkip){
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while(cellIterator.hasNext()){
                        Cell cellN = cellIterator.next();
                        if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                        if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                            continue;
                        } 
                        String str = cellN.getStringCellValue();
                        str=str.trim();
                        if(str.equals("Proposer")){
                            proposerCell=cellCount;
                        }
                        cellCount++;
                    }
                }
                    headSkip=false;
                    continue;
                }
                Cell cell4 = row.getCell(proposerCell, Row.RETURN_BLANK_AS_NULL);
                boolean boolii=false;
                if(cell4.getStringCellValue().trim().equals("student")){
                    boolii = true;
                }
                int count = 0;
                Iterator<Cell> cellIterator = row.cellIterator();
                // Traversing over each cell in a row
                while(cellIterator.hasNext()){
                    Cell cellN = cellIterator.next();
                    if(count<cellCount){
                        count++;
                    }
                    else{
                        if (cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                            continue;
                        }
                        Project p = new Project();
                        String projectTitle = cellN.getStringCellValue().trim();
                        p.setTitle(projectTitle);
                        boolean booli = false;
                        for(Project checkerP : projects){
                            if(checkerP.getTitle().equals(p.getTitle())){
                                booli = true;
                            }
                        }
                        if(!booli){
                            if(!boolii){
                                projects.add(p);
                            }
                            else{
                                projectsProposedByStudents.add(p);
                            }
                        }
                    }
                }
            }
                fis.close();
                workbook.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Couldn't read the file properly");
            System.out.println("in catch for single");
            System.exit(0);
        }
    }
     
    //Method that reads the student information from the single file
    private void readStudentsFromOneFile(File oneFile){
        InputStream fis;
        
        //Try catch block that exits the program with an error message if we can't read the file properly
        try {
            fis = new FileInputStream(oneFile);          
            Workbook workbook = WorkbookFactory.create(fis);
            Sheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                throw new ParseException("No sheet found on index 0", 1);
            }
            Iterator<Row> rowIterator = mySheet.iterator();
            boolean headSkip=true;

            // Traversing over each row of XLSX file
            int studentCell=-1,studentNumCell=-1,gpaCell=-1,proposerCell=-1;
            int cellCount=0;
            while(rowIterator.hasNext()){
                Student student = new Student();
                Row row = rowIterator.next();
                if(checkIfRowEmpty(row)){
                    continue;
                }
                if(headSkip){
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while(cellIterator.hasNext()){
                        Cell cellN = cellIterator.next();
                        if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                        if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                            continue;
                        } 
                        String str = cellN.getStringCellValue();
                        if(str.equals("Student")){
                            studentCell=cellCount;
                            cellCount++;
                        }     
                        if(str.equals("Student Number")){
                            studentNumCell=cellCount;
                            cellCount++;
                        }
                        if(str.equals("GPA")){
                            gpaCell=cellCount;
                            cellCount++;
                        }
                        if(str.equals("Proposer")){
                            proposerCell=cellCount;
                            cellCount++;
                        }
                    }
                }
                    headSkip=false;
                    continue;
                }

                Cell cell1 = row.getCell(studentCell, Row.RETURN_BLANK_AS_NULL);
                Cell cell2 = row.getCell(studentNumCell, Row.RETURN_BLANK_AS_NULL);
                Cell cell3;
                if(gpaCell!=-1){
                    cell3 = row.getCell(gpaCell, Row.RETURN_BLANK_AS_NULL);
                    student.setGpa(cell3.getNumericCellValue());
                }
                else{
                    student.setGpa(1.0);
                }
                Cell cell4 = row.getCell(proposerCell, Row.RETURN_BLANK_AS_NULL);

                student.setFullName(cell1.getStringCellValue().trim());
                int studentNum = (int)cell2.getNumericCellValue();
                student.setStudentNumber(studentNum);
                student.setProposer(cell4.getStringCellValue().trim());

                if(cell4.getStringCellValue().trim().equals("student")){
                    student.setIsProposer(true);
                    studentsWhoProposedTheirProjects.add(student);
                }
                else{
                    students.add(student);
                }

                int count = 0;
                Iterator<Cell> cellIterator = row.cellIterator();

                // Traversing over each cell in a row
                while(cellIterator.hasNext()){
                    Cell cellN = cellIterator.next();
                    if(count<cellCount){
                        count++;
                    }
                    else{
                        if (cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                            continue;
                        }
                        Project p;
                        String projectTitle = cellN.getStringCellValue().trim();
                        if(!student.getIsProposer()){
                        p = projectLookupForOneFile(projectTitle);
                        
                        student.getPreferenceList().add(p);
                        }
                        else{
                            p = projectLookupForProposerStudents(projectTitle);
                            student.getPreferenceList().add(p);
                        }
                    }
                }
            }
            fis.close();
            workbook.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Couldn't read the file properly");
            System.out.println("in catch for single");
            System.exit(0);
        }
    }

    //Method that fills prefrence list of students who give less than 20 preferences, and checks for duplciate student numbers
    private void fixData(){
        for(Student s : students){
            if(s.getPreferenceList().size()<20){
                s.setOriginalNumOfProjects(s.getPreferenceList().size());
            }
            while(s.getPreferenceList().size()<20){
                Random r = new Random();
                int index = r.nextInt(20);
                while(s.getPreferenceList().contains(projects.get(index))){
                    index = r.nextInt(20);
                }
                s.getPreferenceList().add(projects.get(index));
                if(!(this.studentsWithAmendedPreferenceList.contains(s))){
                    this.studentsWithAmendedPreferenceList.add(s);
                }
            }
            if(studentNums.contains(s.getStudentNumber())){
                s.setStudentNumber(counterrr++);
                this.studentsWithAmendedStudentNumber.add(s);
            }
            else{
                studentNums.add(s.getStudentNumber());
            }
        }
    }

    //Method that checks if a row is empty
    private boolean checkIfRowEmpty(Row row){
        boolean bool = false;
        if (String.valueOf(row.getCell(0)).length() == 0 || row.getCell(0) == null){
            bool=true;
        }
        return bool;
    }
    
    //Function that returns the project from the project list that matches the title of the currrent project
    public Project projectLookupForProfessors(String title){
        Project temp = new Project();
        for(Project pro: projects){
            if(pro.getTitle().trim().equalsIgnoreCase(title)){
                temp = pro;
            }
        }
        return temp;
    }

    //Method that returns the project whose title matches the string passed to the method
    private Project projectLookupForOneFile(String title){
        Project temp = new Project();
        for(Project project : projects){
            if(project.getTitle().trim().equals(title)){
                temp = project;
            }
        }
        return temp;
    }

    public Data(){
    }

    //Method that returns project of student who proposd their own projects
    private Project projectLookupForProposerStudents(String title){
        Project temp = new Project();
        for(Project project : projectsProposedByStudents){
            if(project.getTitle().trim().equals(title.trim())){
                temp = project;
            }
        }
        return temp;
    }

    //Function that returns the project from the project list that matches the title and the professor of the current project
    public Project projectLookupForStudents(String title, String professor){
        Project temp = new Project();
        for(Project project : projects){
            if(project.getTitle().trim().equals(title) && project.getProfessor().equals(professor)){
                temp = project;
            }
        }
        return temp;
    }

    //Method that read projects form the projects file
    private List<Project> readExcelProjects(File projectsFile) throws IOException{
        List<Project> projects = new ArrayList<Project>();
        InputStream fis;
    
        //Try catch block that exits the program with an error message if we can't read the file properly
        try {
            fis = new FileInputStream(projectsFile);          
    
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
    
            HSSFSheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                throw new ParseException("No sheet found on index 0", 1);
            }
    
        Iterator<Row> rowIterator = mySheet.iterator();
    
        boolean headSkip=true;
    
        // Traversing over each row of XLSX file
        while(rowIterator.hasNext()){
            Project project = new Project();
            Row row = rowIterator.next();

            //If statement that skips the first row of the file which is the title row
            if(headSkip){
                headSkip=false;
                continue;
            }
            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            Cell cell3 = row.getCell(2, Row.RETURN_BLANK_AS_NULL);

            //Call setters to set project details
            project.setTitle(cell1.getStringCellValue().trim());
            project.setProfessor(cell2.getStringCellValue().trim());
            project.setProjectStream(cell3.getStringCellValue().trim());
            projects.add(project);
            }
            fis.close();
            workbook.close();
            return projects;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Couldn't read the file properly");
            System.out.println("line 495");
            System.exit(0);
        }
        return projects;
    }
    
    //Method that reads the students form the students file
    private List<Student> readExcelStudents(File studentsFile) throws IOException{
        List<Student> students = new ArrayList<Student>();
        InputStream fis;
        //Try catch block that exits the program with an error message if we can't read the file properly
        try {
            fis = new FileInputStream(studentsFile);           
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            HSSFSheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                throw new ParseException("No sheet found on index 0", 1);
            }
        Iterator<Row> rowIterator = mySheet.iterator();
        boolean headSkip=true;
        int rowNum=2;

        // Traversing over each row of XLSX file
        while(rowIterator.hasNext()){  
            Student student = new Student();
            Row row = rowIterator.next();

            //SKip first row of the file
            if(headSkip){
                headSkip=false;
                continue;
        }
        Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
        Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
        Cell cell3 = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
        Cell cell4 = row.getCell(3, Row.RETURN_BLANK_AS_NULL);

        String name = cell1.getStringCellValue();
        String[] names = name.split(" ");
        student.setFirstName(names[0]);
        student.setSurname(names[1]);
        student.setStudentNumber((int)(cell2.getNumericCellValue()));
        student.setStream(cell3.getStringCellValue());
        student.setGpa(cell4.getNumericCellValue());

        int count=0;
        boolean duplicateCheck = false;
        Iterator<Cell> cellIterator = row.cellIterator();
        int duplicateProjects=0;
            // Traversing over each cell in a row
        while(cellIterator.hasNext()){
            boolean bool=false;
            Cell cellN = cellIterator.next();
            if(count<4){
                count++;
            }
            else{
            List<Project> l=student.getPreferenceList();
            String newProject = cellN.getStringCellValue();
            String arr[] = newProject.split(";");
            String titleOfPro = arr[0].trim();
            String nameOfProf = arr[1].trim();
            Project p = projectLookupForStudents(titleOfPro,nameOfProf);

            //Check if there is any duplicates in the preference list, and if there is, break the loop and skip that student 
            if(l.contains(p)){
                duplicateProjects++;
                System.out.println("Student "+student.getStudentNumber()+" has duplicate preferences");
                duplicateCheck=true;
                bool = true;
            } 
            if(!bool){
                l.add(p);
            }
            count++;
            }   
        }

        while(duplicateProjects>0){
            Random rand = new Random();
            int index = rand.nextInt(projects.size());
            Project ppp = projects.get(index);
            while(student.getPreferenceList().contains(ppp)){
                index = rand.nextInt(projects.size());
                ppp = projects.get(index);
            }
            student.getPreferenceList().add(ppp);
            duplicateProjects--;
        }
        //Statement to check if a student has exactly 10 prefrences in the prefrence list
        //If not we skip the student and print the row number
        if(count!=14){
            System.out.println("Row number "+rowNum+" had invalid data");
            System.out.println("Adding random projects to students prefrnce list");
            while(count<14){
                Random rand = new Random();
                int index = rand.nextInt(projects.size());
                Project ppp = projects.get(index);
                while(student.getPreferenceList().contains(ppp)){
                    index = rand.nextInt(projects.size());
                    ppp = projects.get(index);
                }
            student.getPreferenceList().add(ppp);
            count++;
            }
        }
        students.add(student);
        rowNum++;
        }
        fis.close();
        workbook.close();
        return students;
    }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("line 615");
            System.exit(0);
        }
        return students;
    }
    
    //Method that reads the professors from the professors file
    private List<Professor> readExcelProfessors(File professorsFile) throws IOException{
        List<Professor> professors = new ArrayList<Professor>();
        InputStream fis;

        try {
            fis = new FileInputStream(professorsFile);             
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            HSSFSheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                throw new ParseException("No sheet found on index 0", 1);
            }
            Iterator<Row> rowIterator = mySheet.iterator();
            boolean headSkip=true;

            // Traversing over each row of XLSX file
            while (rowIterator.hasNext()) {
                Professor professor = new Professor();
                Row row = rowIterator.next();
                if(headSkip){
                    headSkip=false;
                    continue;
                }
                Cell name = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
                Cell researchField = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
                professor.setName(name.getStringCellValue());
                professor.setInterest(researchField.getStringCellValue());
                int count = 0;
                Iterator<Cell> cellIterator = row.cellIterator();

                // Traversing over each cell in a row
                while(cellIterator.hasNext()){
                    Cell cellN = cellIterator.next();
                    if(count<2){
                        count++;
                    }
                    else{
                    String line = cellN.getStringCellValue();
                    String[] seperator = line.split("-");
                    String projectTitle = seperator[0].trim();
                    professor.getProjects().add(projectLookupForProfessors(projectTitle));
                    }
                }
                professors.add(professor);
            }
            fis.close();
            workbook.close();
            return professors;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Couldn't read the file properly " );
            System.out.println("line 682");
            System.exit(0);
        }
        return professors;
    }

    public String studentEdits(){
        String edits = "\nAmendments made to data:\n";
        edits += "The following students had less than 20 projects. Projects were allocated to them as a result:\n";
        for(Student student : studentsWithAmendedPreferenceList){
            edits += student.getFullName() + "\n";
        }
        edits += "\n\nThe following students had an already assigned student number, and were assigned a new one:\n";
        for(Student student : studentsWithAmendedStudentNumber){
            edits += student.getFullName() + "was assigned " + student.getStudentNumber() + "\n";
        }
        return edits;
    }

    public List<Student> getStudentsWithAmendedPreferenceList() {
        return studentsWithAmendedPreferenceList;
    }

    public List<Student> getStudentsWhoProposedTheirProject(){
        return studentsWhoProposedTheirProjects;
    }

    public void setStudentsWithAmendedPreferenceList(List<Student> studentsWithAmendedPreferenceList) {
        this.studentsWithAmendedPreferenceList = studentsWithAmendedPreferenceList;
    }

    public List<Student> getStudentsWithAmendedStudentNumber() {
        return studentsWithAmendedStudentNumber;
    }

    public void setStudentsWithAmendedStudentNumber(List<Student> studentsWithAmendedStudentNumber) {
        this.studentsWithAmendedStudentNumber = studentsWithAmendedStudentNumber;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Professor> getProfessors() {
        return professors;
    }

    public void setProfessors(List<Professor> professors) {
        this.professors = professors;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
