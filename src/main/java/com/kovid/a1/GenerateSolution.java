package com.kovid.a1;
import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class GenerateSolution {
    private int count=0;
    private List<Project> projects;
    private List<Student> students;
    private List<Project> allocatedProjects = new ArrayList<Project>();
    private HashMap<Project, Student> solution = new HashMap<Project, Student>();
    private HashMap<Project, Student> bestSolution = new HashMap<Project, Student>();
    
    GenerateSolution(List<Project> projects, List<Student> students) throws IOException {
        this.projects = projects;
        this.students = students;

        //As we randomly pick a student and assign a project to them, we have a loop that runs until we find a solution where all students get allocated a project
        for(int loop = 0;loop<10000;loop++){
            randomAllocation();
            count=solution.size();

            //Take best current solution, either the new one generated, or the current, whichever assigns more students projects
            if(bestSolution.size()<solution.size()){
                bestSolution.clear();
                for (Map.Entry<Project, Student> entry : solution.entrySet()){
                bestSolution.put(entry.getKey(),solution.get(entry.getKey()));
                }
            }
            //As soon as we find a solution where all students are allocated a project, we stop
            if(count==students.size()){
                break;
            }
            else{
                if(loop!=9999){
                    solution.clear();
                    allocatedProjects.clear();
                }
            }
        }
    }

    //Randomly attempt to assign a project from a student's preference list
    private void randomAllocation() throws IOException {
        Random r = new Random();
       
        for(Student currStudent : students){
            int randomInt = r.nextInt(currStudent.getPreferenceList().size());
            Project pro = currStudent.getPreferenceList().get(randomInt);
            if(!allocatedProjects.contains(pro)){
                solution.put(pro, currStudent);
                allocatedProjects.add(pro);
            }
            else{
                projectAlreadyAllocatedCase(currStudent);
            }
        }
    }
    //If the random selection from a preference list is already taken by another student, try all other projects randomly
    //Keeps track of preference list entries that are tried, to avoid unsuccessful repetition
    private void projectAlreadyAllocatedCase(Student currStudent){
        boolean check = true;
        Random r = new Random();
        List<Integer> attempts = new ArrayList<Integer>();
        while(attempts.size()!=currStudent.getPreferenceList().size()){
            int randomIndex = r.nextInt(currStudent.getPreferenceList().size());
            if(!(attempts.contains(randomIndex))){
                attempts.add(randomIndex);
                Project pro = currStudent.getPreferenceList().get(randomIndex);
                if(!allocatedProjects.contains(pro)){
                solution.put(pro, currStudent);
                allocatedProjects.add(pro);
                check = false;
                break;
                }  
            }
        }
        if(check){
            allProjectsAlreadyAllocated(currStudent);
        }
    }

    //If a project cannot be assigned, attempts to unnasign one from an existing pairing, reassigning that student's to another from their list.
    private void allProjectsAlreadyAllocated(Student currStudent){

        boolean checker = false;
        for(int i = 0;i<currStudent.getPreferenceList().size();i++){
            Project pro = currStudent.getPreferenceList().get(i);
            Student stu = solution.get(pro);
            for(int j = 0;j<stu.getPreferenceList().size();j++){
                Project proj = stu.getPreferenceList().get(i);
                if(!allocatedProjects.contains(proj)){
                    // solution.remove(pro);
                    solution.put(proj, stu);
                    allocatedProjects.add(proj);
                    solution.put(pro, currStudent);
                    checker = true;
                    break;
                }
            }
            if(checker){
                break;
            }
        }

    }

    //Function that writes the solution found to a .xlsx file
    private void writeToFile() throws IOException {

        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        int rowCount = 0;
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Student Name");
        header.createCell(1).setCellValue("Allocated Project");

        for (Project p : bestSolution.keySet()) {
            Student s = bestSolution.get(p);
            Row row = sheet.createRow(++rowCount);

            String str = s.getFirstName()+" "+s.getSurname();
            Cell cell = row.createCell(0);
            cell.setCellValue(str);
            cell = row.createCell(1);
            cell.setCellValue(p.getTitle());

          }

        try (FileOutputStream outputStream = new FileOutputStream("src/main/resources/solution700Students.xlsx")) {
            workbook.write(outputStream);
            workbook.close();
        }

    }

    public String toString(){
        String str = "";
        int i = 1;

        for (Project p : bestSolution.keySet()) {
            Student s = bestSolution.get(p);
            str += i + ": " + s.getFirstName() + " " + s.getSurname() + " Project: " + p.getTitle() + "\n";
            i++;
        }

        return str;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Project> getAllocatedProjects() {
        return allocatedProjects;
    }

    public void setAllocatedProjects(List<Project> allocatedProjects) {
        this.allocatedProjects = allocatedProjects;
    }

    public HashMap<Project, Student> getSolution() {
        return solution;
    }

    public void setSolution(HashMap<Project, Student> solution) {
        this.solution = solution;
    }

    public HashMap<Project, Student> getBestSolution() {
        return bestSolution;
    }

    public void setBestSolution(HashMap<Project, Student> bestSolution) {
        this.bestSolution = bestSolution;
    }
}