package com.kovid.a1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class GeneticAlgorithm {

    private List<Solution> solutions;
    private int cullPercentage, matePercentage, vacantEntries, mutatePercentage, sampleSizeChocie;
    private double splitWeight, gpaWeight;
    private File solutionFile;
    private String str="";
    private VBox output;
    private Stage window;
    private Scene geneticAlgorithmScene;
    private Boolean singleFileChoice;

    public GeneticAlgorithm(Boolean singleFileChoice, String edits, List<Solution> solutions, int matePercentage, int cullPercentage, int mutatePercentage, double splitWeight, double gpaWeight, int sampleSizeChocie, VBox output, Stage window, Scene geneticAlgorithmScene)
            throws IOException {

        this.singleFileChoice = singleFileChoice;
        this.cullPercentage = cullPercentage;
        this.matePercentage = matePercentage;
        this.solutions = solutions;
        this.vacantEntries = 0;
        this.mutatePercentage = mutatePercentage;
        this.splitWeight = splitWeight;
        this.gpaWeight = gpaWeight;
        this.sampleSizeChocie = sampleSizeChocie;
        this.output = output;
        this.window = window;
        this.geneticAlgorithmScene = geneticAlgorithmScene;
        
        //Call the method that starts the GA process
        startGA();
    }
    public GeneticAlgorithm(List<Solution> solutions, int matePercentage, int cullPercentage, int mutatePercentage, double splitWeight, double gpaWeight, int sampleSizeChocie)
            throws IOException {
        this.cullPercentage = cullPercentage;
        this.matePercentage = matePercentage;
        this.solutions = solutions;
        this.vacantEntries = 0;
        this.mutatePercentage = mutatePercentage;
        this.splitWeight = splitWeight;
        this.gpaWeight = gpaWeight;
        this.sampleSizeChocie = sampleSizeChocie;

        //Call the method that starts the GA process
        startGA();
    }
    
    private void startGA(){
        double bestFitness=1.0;
        boolean keepRunning = true;
        int numOfNotGoodChangesInARow = 0;
        int generation = 1;
        //We keep running our GA process until we get to a point where the Mating doesn't have a effect on our fitness for mutiple calls
        while(keepRunning){
            System.out.println("Generation: " + generation + " Fitness: " + solutions.get(0).getFitness() + " Generations since improvement: " + numOfNotGoodChangesInARow);
            str="Generation: " + generation + " Fitness: " + solutions.get(0).getFitness() + " Generations since improvement: " + numOfNotGoodChangesInARow+"\n";
            ScrollPane sp = (ScrollPane)output.getChildren().get(1);
            TextFlow tf = (TextFlow) sp.getContent();
            Text text = new Text(str);
            Platform.runLater(new Runnable(){

                @Override
                public void run() {
                    tf.getChildren().add(text);
                }
            });
            
            organiseByFitness();
            cull(solutions);
            mateCandidates();
            mutate();
            organiseByFitness();
            double currBestFitness = solutions.get(0).getFitness();

            if(currBestFitness<bestFitness){
                numOfNotGoodChangesInARow++;
            }
            else{    
                double increaseInFitness = currBestFitness - bestFitness;
                if((increaseInFitness/bestFitness)*100 < 1){
                    numOfNotGoodChangesInARow++;
                }
                else{
                    numOfNotGoodChangesInARow=0;
                }
                bestFitness = currBestFitness;
            }
            if(numOfNotGoodChangesInARow>15){
                keepRunning=false;
            }
            generation++;
        }
    }

    private void organiseByFitness(){
        //We have a class FitnessSort that implements the comparator to sort our solutions list based on fitness
        solutions.sort(new FitnessSort());
    }
    
    //This method culls a set percentage of solutions form our list
    private void cull(List<Solution> solutions){
        int size = solutions.size();
        int index = (size*cullPercentage)/100;
        for(int i=size-1;i>=(size-index);i--){
            solutions.remove(i);
            vacantEntries++;
        }
    }

    //This function mates a set percentage of candidates by calling the crossover method with two parent solutions as arguments
    private void mateCandidates(){
        List<Solution> candidates = new ArrayList<Solution>();
        int numOfCandidates = 0;

        for(int i=0; i<(solutions.size()*matePercentage)/100; i++){
            candidates.add(solutions.get(i));
        }
        numOfCandidates = candidates.size();
        int limit = 0;
        Random r = new Random();

        //While loop that executes until all empty spots in the solutions list are filled(after some were culled)
        while(vacantEntries>0){
            int randomCandidate1  = r.nextInt(candidates.size());
            int randomCandidate2  = r.nextInt(candidates.size());
            if(limit<numOfCandidates){
                while(limit==randomCandidate1){
                    randomCandidate1 = r.nextInt(candidates.size());
                }
                Crossover crossover = new Crossover(candidates.get(limit), candidates.get(randomCandidate1), splitWeight, sampleSizeChocie);
                Solution offspring = crossover.getOffSpring();

                //check for validity of solution here
                if(offspring.getIsValidOffspring()==false){
                    vacantEntries++;
                }
                else{
                    offspring.setFitness(offspring.calculateFitness(gpaWeight));
                    solutions.add(offspring);
                }
                limit++;
            }
            else{
                while(randomCandidate1==randomCandidate2){
                    randomCandidate2 = r.nextInt(candidates.size());
                }
    
                Crossover crossover = new Crossover(candidates.get(randomCandidate1), candidates.get(randomCandidate2), splitWeight, sampleSizeChocie);
                Solution offspring = crossover.getOffSpring();
                //check here for validity of solution
                if(offspring.getIsValidOffspring()==false){
                    vacantEntries++;
                }
                else{
                    offspring.setFitness(offspring.calculateFitness(gpaWeight));
                    solutions.add(offspring);
                }
            }

            vacantEntries--;
        }
    }

  
    //Method that mutates a set percentage of the solution by making ranodm chanegs to ti
    private void mutate() {
        int numberOfMutations = (mutatePercentage*solutions.size())/100;
        int i = 0;
        List<Integer> alreadyVisited = new ArrayList<Integer>();
        while(i<numberOfMutations){
            Random r = new Random();
            int index = r.nextInt(solutions.size());
            if(!alreadyVisited.contains(index)){
                alreadyVisited.add(index);
                Solution s = solutions.get(index);
                s.randomChange();
                i++;
            }
        }
        alreadyVisited.clear();
    }

    
    public void writeToFile(File file) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        int rowCount = 0;
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Student Name");
        header.createCell(1).setCellValue("Allocated Project");
        for (Project p : solutions.get(0).getSolutionData().keySet()) {
                Student s = solutions.get(0).getSolutionData().get(p);
                Row row = sheet.createRow(++rowCount);

                Cell cell = row.createCell(0);
                cell.setCellValue(s.getFullName());
                cell = row.createCell(1);
                cell.setCellValue(p.getTitle());

          }

        // try (FileOutputStream outputStream = new FileOutputStream("src/main/resources/GAGUISolution.xlsx")) {
        try (FileOutputStream outputStream = new FileOutputStream(file.getPath())) {

            workbook.write(outputStream);
            workbook.close();
         
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<Solution> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<Solution> solutions) {
        this.solutions = solutions;
    }

    public int getCullPercentage() {
        return cullPercentage;
    }

    public void setCullPercentage(int cullPercentage) {
        this.cullPercentage = cullPercentage;
    }

    public int getMatePercentage() {
        return matePercentage;
    }

    public void setMatePercentage(int matePercentage) {
        this.matePercentage = matePercentage;
    }

    public int getVacantEntries() {
        return vacantEntries;
    }

    public void setVacantEntries(int vacantEntries) {
        this.vacantEntries = vacantEntries;
    }

    public int getMutatePercentage() {
        return mutatePercentage;
    }

    public void setMutatePercentage(int mutatePercentage) {
        this.mutatePercentage = mutatePercentage;
    }

    public double getSplitWeight() {
        return splitWeight;
    }

    public void setSplitWeight(double splitWeight) {
        this.splitWeight = splitWeight;
    }

    public double getGpaWeight() {
        return gpaWeight;
    }

    public void setGpaWeight(double gpaWeight) {
        this.gpaWeight = gpaWeight;
    }

    public int getSampleSizeChocie() {
        return sampleSizeChocie;
    }

    public void setSampleSizeChocie(int sampleSizeChocie) {
        this.sampleSizeChocie = sampleSizeChocie;
    }

    public File getSolutionFile() {
        return solutionFile;
    }

    public void setSolutionFile(File solutionFile) {
        this.solutionFile = solutionFile;
    }


}