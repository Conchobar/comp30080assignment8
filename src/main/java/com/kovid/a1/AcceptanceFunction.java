package com.kovid.a1;

public interface AcceptanceFunction {

    //Function that is implemented to use Boltzman or a simple distribution
    public double acceptanceFunction(double energy1, double energy2, double temperature);

    //Function that returns a boolean indicating whether a cnage should be accepted or not
    public Boolean acceptOrNot(double energy1, double energy2,  double temperature);

    public String getName();

    public void setName(String name);
}