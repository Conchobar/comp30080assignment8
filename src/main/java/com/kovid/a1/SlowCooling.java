package com.kovid.a1;

public class SlowCooling implements CoolingSchedule{

    private int maxTemperature;
    private double temperature;
    private double coolingRate;
    private int iterations;
    private String name;

    //Default constructor
    public SlowCooling(){

        this.maxTemperature = 100;
        this.temperature = maxTemperature;
        this.coolingRate = .1;
        this.iterations = 100;
        this.name = "Slow Cooling";
    }

    //Parameterized constructor that initializes the instance variables
    public SlowCooling(double temperature, double coolingRate){
        this.temperature = temperature;
        this.coolingRate = coolingRate;
    }

    //Function that returns a boolean indicating if the program should stop
    public Boolean stopOrNot(){
        if(temperature <= 0){
            temperature = maxTemperature;
            return true;
        }
        else{
            return false;
        }
    }

    public void reduceTemperature(){
        temperature-=coolingRate;
        iterations=0;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getCoolingRate() {
        return coolingRate;
    }

    public void setCoolingRate(double coolingRate) {
        this.coolingRate = coolingRate;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

}