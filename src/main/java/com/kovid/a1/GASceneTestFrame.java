package com.kovid.a1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;


//Class that creates the test frame for GAscene
public class GASceneTestFrame extends FileIO {
    private static int sampleSizeChoice = 0, sampleSize = 1000, mutateWeight = 10, cullWeight = 10;
    private static int mateWeight = 20;
    private int dataSetType = 1;
    private static double splitWeight = 50;
    private static double gpaWeight = 50;
    private static Stage window;
    private static Scene welcomeScene;
    private static Scene geneticAlgorithmScene;
    private static Button writeGa, showSolution, showSolutionBeforeGA, help;
    private static ProgressBar solutionBar;
    private static File studentsFile;
    private static File projectsFile;
    private static File professorsFile;
    private static File solutionFile;
    private static File singleFile;
    private static Data data;
    private static VBox output;
    private static VBox vbox, progressBox;
    private static String edits;
    private static boolean threeFilesChoice = false;
    private static boolean singleFileChoice = false;

    // Parameterized constructor to set the stage and scene
    GASceneTestFrame(Stage window, Scene welcomeScene) {
        this.window = window;
        this.welcomeScene = welcomeScene;
    }

    // Method tha creates the Genetic algorithm and returns it to be displayed
    public static Scene geneticAlgorithmScene() {
        // Create a splitpane container that has the GUI on one side and the output on
        // the other
        SplitPane splitPane = new SplitPane();
        VBox gaBox = new VBox();

        TextFlow tf = new TextFlow();
        tf.setPrefHeight(1280);
        tf.setMaxWidth(1600);
        tf.setPrefWidth(720);
        tf.setBackground(new Background(new BackgroundFill(Color.rgb(255, 255, 255), null, null)));

        ScrollPane scrollPane = new ScrollPane(tf);
        scrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);

        Button run = new Button("Run");
        run.setId("run");
        run.setDisable(true);

        output = new VBox(new Label("Output"), scrollPane);
        geneticAlgorithmScene = new Scene(gaBox);

        // Button that uses the FileChooser object to import the students file to the
        // application
        Button studentFileButton = new Button("Add Students File");
        studentFileButton.setId("studentsFileButton");
        Label studentFileLabel = new Label("No File Selected");
        HBox studentFileInfo = new HBox(studentFileButton, studentFileLabel);
        studentFileButton.setOnAction(value -> {
            String message = "Correct";
            FileChooser studentsChooser = new FileChooser();
            setStudentsFile(studentsChooser.showOpenDialog(window));
            studentFileLabel.setText(getStudentsFile().getName());
            if ((!(projectsFile == null) && (!(professorsFile == null)))) {
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (!(message.equals("Correct"))) {
                final Stage myDialog = new Stage();
                myDialog.setTitle("Student File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);

            } else if ((message.equals("Correct")) && (!(projectsFile == null)) && (!(professorsFile == null))) {
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;

            }
        });

        Button projectsFileButton = new Button("Add Projects File");
        projectsFileButton.setId("projectsFileButton");
        Label projectsFileLabel = new Label("No File Selected");
        HBox projectsFileInfo = new HBox(projectsFileButton, projectsFileLabel);
        projectsFileButton.setOnAction(value -> {
            FileChooser projectsChooser = new FileChooser();
            setProjectsFile(projectsChooser.showOpenDialog(window));
            projectsFileLabel.setText(getProjectsFile().getName());
            String message = "Correct";
            if ((!(studentsFile == null) && (!(professorsFile == null)))) {
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (!(message.equals("Correct"))) {
                final Stage myDialog = new Stage();
                myDialog.setTitle("Project File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);

            } else if ((message.equals("Correct")) && (!(studentsFile == null)) && (!(professorsFile == null))) {
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;

            }
        });

        // Button that uses the FileChooser object to import the professors file to the
        // application
        Button professorsFileButton = new Button("Add Professors File");
        professorsFileButton.setId("professorsFileButton");
        Label professorsFileLabel = new Label("No File Selected");
        HBox professorFileInfo = new HBox(professorsFileButton, professorsFileLabel);
        professorsFileButton.setOnAction(value -> {
            String message = "Correct";
            FileChooser proffesorsChooser = new FileChooser();
            setProfessorsFile(proffesorsChooser.showOpenDialog(window));
            professorsFileLabel.setText(getProfessorsFile().getName());
            if ((!(projectsFile == null) && (!(studentsFile == null)))) {
                threeFilesChoice = true;
                singleFileChoice = false;
                try {
                    message = FileIO.threeFilesValid(studentsFile, projectsFile, professorsFile);
                } catch (IOException | ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (!(message.equals("Correct"))) {
                final Stage myDialog = new Stage();
                myDialog.setTitle("Professor File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);
            } else if ((message.equals("Correct")) && (!(studentsFile == null)) && (!(projectsFile == null))) {
                run.setDisable(false);
                singleFileChoice = false;
                threeFilesChoice = true;
            }
        });

        Text t = new Text();
        t.setText("\t    OR");
        t.setFont(Font.font("Verdana", FontWeight.BOLD, 30));
        t.setFill(Color.BLUE);

        Button singleFileButton = new Button("Add Single File");
        singleFileButton.setId("singleFileButton");
        Label singleFileLabel = new Label("No File Selected");
        HBox singleFileBox = new HBox(singleFileButton, singleFileLabel);
        singleFileButton.setOnAction(value -> {
            FileChooser singleFileChooser = new FileChooser();
            setSingleFile(singleFileChooser.showOpenDialog(window));
            singleFileLabel.setText(getSingleFile().getName());
            threeFilesChoice = false;
            singleFileChoice = true;

            // Validate file
            String message = "Correct";

            try {
                message = FileIO.isFileValid(singleFile);
            } catch (IOException | ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (!(message.equals("Correct"))) {
                final Stage myDialog = new Stage();
                myDialog.setTitle("Single File Error");
                myDialog.initModality(Modality.WINDOW_MODAL);
                Button okButton = new Button("CLOSE");
                okButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent arg0) {
                        myDialog.close();
                    }
                });
                HBox welcomeBox = new HBox();
                Label messageLabel = new Label(message);
                messageLabel.minWidth(500);
                messageLabel.prefWidth(500);
                messageLabel.setFont(new Font("Arial", 24));
                welcomeBox.getChildren().add(messageLabel);
                Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                myDialog.setScene(myDialogScene);
                myDialog.show();
                run.setDisable(true);
            } else {
                run.setDisable(false);
                singleFileChoice = true;
                threeFilesChoice = false;
            }
        });

        showSolution = new Button("Display Solution After GA");
        showSolutionBeforeGA = new Button("Display Solution Before GA");
        // Sliders for user input
        // Slider to set the mate percentage
        Label sampleSizeLabel = new Label("Sample Size");
        Slider sampleSizeSlider = new Slider(1000, 10000, 1000);
        sampleSizeSlider.setId("sampleSizeSlider");
        sampleSizeSlider.setMajorTickUnit(1000);
        sampleSizeSlider.setMinorTickCount(100);
        sampleSizeSlider.setSnapToTicks(true);
        sampleSizeSlider.setShowTickMarks(true);
        sampleSizeSlider.setShowTickLabels(true);

        Label mateLabel = new Label("Mate Percentage");
        Slider mateSlider = new Slider(1, 99, 20);
        mateSlider.setId("mateSlider");
        mateSlider.setMajorTickUnit(10.0);
        mateSlider.setMinorTickCount(10);
        mateSlider.setSnapToTicks(true);
        mateSlider.setShowTickMarks(true);
        mateSlider.setShowTickLabels(true);

        // Slider to set the cull percentage
        Label culLabel = new Label("Cull Percentage");
        Slider cullSlider = new Slider(1, 99, 20);
        cullSlider.setId("cullSlider");
        cullSlider.setMajorTickUnit(10.0);
        cullSlider.setMinorTickCount(10);
        cullSlider.setSnapToTicks(true);
        cullSlider.setShowTickMarks(true);
        cullSlider.setShowTickLabels(true);

        // Slider to set the mutate percentage
        Label mutateLabel = new Label("Mutate Percentage");
        Slider mutateSlider = new Slider(0, 20, 10);
        mutateSlider.setId("mutateSlider");
        mutateSlider.setMajorTickUnit(10.0);
        mutateSlider.setMinorTickCount(10);
        mutateSlider.setSnapToTicks(true);
        mutateSlider.setShowTickMarks(true);
        mutateSlider.setShowTickLabels(true);

        // Slider to set the split percentage
        Label splitLabel = new Label("Split Percentage");
        Slider splitSlider = new Slider(1, 99, 50);
        splitSlider.setId("splitSlider");
        splitSlider.setMajorTickUnit(10.0);
        splitSlider.setMinorTickCount(10);
        splitSlider.setSnapToTicks(true);
        splitSlider.setShowTickMarks(true);
        splitSlider.setShowTickLabels(true);

        // Slider to set the GPA percentage
        Label gpaLabel = new Label("GPA Percentage");
        Slider gpaSlider = new Slider(0, 100, 50);
        gpaSlider.setId("gpaSlider");
        gpaSlider.setMajorTickUnit(10.0);
        gpaSlider.setMinorTickCount(10);
        gpaSlider.setSnapToTicks(true);
        gpaSlider.setShowTickMarks(true);
        gpaSlider.setShowTickLabels(true);

        // Listeners that keep track of user input
        sampleSizeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                sampleSize = new_val.intValue();
                System.out.println("New mateSlider Value is " + mateWeight);
            }
        });

        mateSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                mateWeight = new_val.intValue();
                System.out.println("New mateSlider Value is " + mateWeight);
            }
        });

        cullSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                cullWeight = new_val.intValue();
            }
        });

        mutateSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                mutateWeight = new_val.intValue();
            }
        });

        splitSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                splitWeight = new_val.intValue();
            }
        });

        gpaSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                gpaWeight = new_val.intValue();
            }
        });

        Label progressLabel = new Label("Creating Initial Solutions:");
        solutionBar = new ProgressBar(0);
        progressBox = new VBox(progressLabel, solutionBar);
        progressBox.setVisible(false);

        // When the user clicks Run, we check if we have valid data, if not we give an
        // error message
        // If data is valid, we execute genetic algorithm on the data
        run.setOnAction(value -> {
            run.setText("Clicked");

            try {
                System.out.println("before data obj created grand");
                if (threeFilesChoice) {
                    data = new Data(projectsFile, studentsFile, professorsFile);
                } else if (singleFileChoice) {
                    data = new Data(singleFile);
                }

                // data = new Data(dataSetChoice);
                String message = isDataValid(data);
                if (!(message.equals("Correct"))) {
                    final Stage myDialog = new Stage();
                    myDialog.initModality(Modality.WINDOW_MODAL);
                    Button okButton = new Button("CLOSE");
                    okButton.setOnAction(new EventHandler<ActionEvent>() {
                        public void handle(ActionEvent arg0) {
                            myDialog.close();
                        }
                    });
                    HBox welcomeBox = new HBox();
                    welcomeBox.getChildren().add(new Label("Invalid data files, select files again"));
                    Scene myDialogScene = new Scene(welcomeBox, 250, 50);
                    myDialog.setScene(myDialogScene);
                    myDialog.show();
                } else {
                    Runnable task = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                System.out.println("gets to line 337");
                                runGeneticAlgorithm(studentsFile, projectsFile, professorsFile, sampleSize, mateWeight,
                                        cullWeight, mutateWeight, splitWeight, gpaWeight, output);
                            } catch (IOException e) {
                                System.out.println("error in try catch at 336");
                                e.printStackTrace();
                            }
                        }
                    };
                    Thread backgroundThread = new Thread(task);
                    backgroundThread.setDaemon(true);
                    backgroundThread.start();
                    // }
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("error in try catch at 351");
            }
        });

        // Button that can only be clicked once, we have generated a solution
        writeGa = new Button("Write to File");
        writeGa.setId("write");
        writeGa.setDisable(true);
        showSolution.setDisable(true);
        showSolutionBeforeGA.setDisable(true);
        Button back = new Button("Back");
        back.setId("back");
        back.setOnAction(value -> {
            back.setText("Clicked");
            // window.setScene(welcomeScene);
            // window.setTitle("Home");
            // window.show();
        });

        help = new Button("Help");
        help.setId("help");
        help.setOnAction(value -> {
            final Stage myDialog = new Stage();
            myDialog.setTitle("Help for Genetic Algorithm");
            myDialog.initModality(Modality.WINDOW_MODAL);
            Button okButton = new Button("CLOSE");
            okButton.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent arg0) {
                    myDialog.close();
                }
            });

            // InputStream is;
            // is = (this.getClass().getResourceAsStream("/GAInfo.txt"));
            FileInputStream fis = null;
            try {
                fis = new FileInputStream("/GAInfo.txt");
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append('\n');
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String information = sb.toString();
            Text infoText = new Text(information);
            TextFlow text = new TextFlow();
            text.getChildren().add(infoText);
            Scene scene = new Scene(text);
            myDialog.setScene(scene);
            myDialog.setHeight(600);
            myDialog.setWidth(600);
            myDialog.show();
        });

        ScrollBar sc = new ScrollBar();
        sc.setOrientation(Orientation.VERTICAL);
        sc.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                vbox.setLayoutY(-new_val.doubleValue());
            }
        });

        vbox = new VBox(studentFileInfo, projectsFileInfo, professorFileInfo, t, singleFileBox, sampleSizeLabel,
                sampleSizeSlider, mateLabel, mateSlider, culLabel, cullSlider, mutateLabel, mutateSlider, splitLabel,
                splitSlider, gpaLabel, gpaSlider, run, progressBox, showSolutionBeforeGA, showSolution, writeGa, back,
                help);
        vbox.setSpacing(5);
        ScrollPane sp = new ScrollPane(vbox);
        splitPane.getItems().addAll(sp, output);
        geneticAlgorithmScene = new Scene(splitPane, 1280, 720);
        return geneticAlgorithmScene;
    }

    // Method that runs the GA algorithm by creating a GeneticAlgorithm object
    private static void runGeneticAlgorithm(File studentsFile, File projectsFile, File professorsFile, int sampleSize,
            int mateWeight, int cullWeight, int mutateWeight, double splitWeight, double gpaWeight, VBox output)
            throws IOException {
        System.out.println("run GA called ok");
        solutionBar.setPrefWidth(400);
        progressBox.setVisible(true);
        writeGa.setDisable(true);
        gpaWeight /= 100;
        splitWeight /= 100;

        String avgPreferences = "";
        int sizeP = sampleSize;
        ScrollPane sp = (ScrollPane) output.getChildren().get(1);
        TextFlow tf = (TextFlow) sp.getContent();
        sampleSizeChoice = data.getStudents().size();
        ArrayList<Solution> solutionList = new ArrayList<Solution>();
        ArrayList<Solution> copyList = new ArrayList<Solution>();
        Data copy = new Data();
        // Create a 1000 solution to run GA
        if (threeFilesChoice) {
            for (int i = 0; i < sizeP; i++) {
                Data mockData = new Data(projectsFile, studentsFile, professorsFile);
                Solution solution = new Solution(mockData);
                // avgPreferences += "Before running Genetic Algorithm Average Preference is: ";
                // avgPreferences += solution.returnAveragePreference() + "\n";
                if (solution.getSolutionData().size() < sampleSizeChoice) {
                    i--;
                } else {
                    solution.setTemperature(solution.calculateTemperature(gpaWeight));
                    solution.setFitness(solution.calculateFitness(gpaWeight));
                    solutionList.add(solution);
                    copyList.add(solution);
                    System.out.println("Solutions generated: " + solutionList.size());
                    double progress = solutionList.size();
                    solutionBar.setProgress(progress / sizeP);
                }
            }
        } else if (singleFileChoice) {
            for (int i = 0; i < sizeP; i++) {
                // System.out.println("line 421 ok");
                Data mockData = new Data(singleFile);
                edits = mockData.studentEdits();

                copy = mockData;
                // System.out.println(data.getStudents().toString());
                Solution solution = new Solution(mockData);
                // if(i==0){
                // System.out.println(solution.toString());
                // System.exit(0);
                // }

                if (solution.getSolutionData().size() < data.getStudents().size()) {
                    i--;
                } else {
                    solution.setTemperature(solution.calculateTemperature(gpaWeight));
                    solution.setFitness(solution.calculateFitness(gpaWeight));
                    solutionList.add(solution);
                    copyList.add(solution);
                    System.out.println("Solutions generated: " + solutionList.size());
                    double progress = solutionList.size();
                    solutionBar.setProgress(progress / sizeP);
                }
            }
        }
        // else{
        // System.out.println("data called with datasetchoice");
        // for(int i = 0; i<sizeP; i++){
        // Data mockData = new Data(dataSetChoice);
        // edits = mockData.studentEdits();
        // Solution solution = new Solution(mockData);
        // if(solution.getSolutionData().size()<sampleSizeChoice){
        // i--;
        // }
        // else{
        // solution.setTemperature(solution.calculateTemperature(gpaWeight));
        // solution.setFitness(solution.calculateFitness(gpaWeight));
        // solutionList.add(solution);
        // copyList.add(solution);
        // System.out.println("Solutions generated: " + solutionList.size());
        // double progress = solutionList.size();
        // solutionBar.setProgress(progress/sizeP);
        // }
        // }
        // }
        avgPreferences += "\nBefore running Genetic Algorithm Average Preference was: ";
        if (singleFileChoice) {
            avgPreferences += solutionList.get(0).returnAveragePreferenceForSingleFile() + "\n";
        } else {
            avgPreferences += solutionList.get(0).returnAveragePreference() + "\n";
        }

        // Create a GeneticAlgorithm object using the solution list created
        GeneticAlgorithm ga = new GeneticAlgorithm(singleFileChoice, edits, solutionList, mateWeight, cullWeight,
                mutateWeight, splitWeight, gpaWeight, sampleSizeChoice, output, window, geneticAlgorithmScene);
        copyList.sort(new FitnessSort());
        Solution originalBestSolution = copyList.get(0);
        Solution bestSolution = solutionList.get(0);

        displaySolutionBeforeGA(copyList.get(0));
        displaySolution(solutionList.get(0));

        avgPreferences += "After running Genetic Algorithm Average Preference is: ";
        if (!(singleFileChoice)) {
            avgPreferences += bestSolution.returnAveragePreference() + "\n";
        } else {
            avgPreferences += bestSolution.returnAveragePreferenceForSingleFile() + "\n";
        }

        Text avgPrefText = new Text(avgPreferences);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                tf.getChildren().add(avgPrefText);
            }
        });

        System.out.println("From the best solution before GA - ");
        if (!singleFileChoice) {
            originalBestSolution.averagePreference();
        } else {
            originalBestSolution.averagePreferenceForSingleFile();
        }
        System.out.println("From the best solution after GA - ");
        if (!singleFileChoice) {
            bestSolution.averagePreference();
        } else {
            bestSolution.averagePreferenceForSingleFile();
        }
        if (singleFileChoice) {
            List<Student> l = copy.getStudentsWhoProposedTheirProject();
            for (Student s : l) {
                Project p = s.getPreferenceList().get(0);
                bestSolution.getSolutionData().put(p, s);
            }
        }
        edits += "\nStudents who got a project that was not in their preference list, due to giving an incomplete prefernece list are - \n";
        for (Project p : bestSolution.getSolutionData().keySet()) {
            Student s = bestSolution.getSolutionData().get(p);
            if (s.getOriginalNumOfProjects() < 20) {
                int index = s.getPreferenceList().indexOf(p);
                if (index >= s.getOriginalNumOfProjects()) {
                    edits += s.getFullName() + " gaove only " + s.getOriginalNumOfProjects() + " preferences" + "\n";
                }
            }
        }
        FileIO.printSolution(bestSolution, output, edits, singleFileChoice);

        // ga.screenPrint(ga.getSolutions().get(0), output);
        System.out.println("mateWeight: " + mateWeight + " cullWeight: " + cullWeight + " mutateWeight: " + mutateWeight
                + " splitWeight: " + splitWeight + " gpaWeight " + gpaWeight);

        writeGa.setDisable(false);

        // Use a FileChooser object to let the user select the path where they want to
        // save the solution
        FileChooser solutionChooser = new FileChooser();
        solutionChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLSX Files (*.xlsx)", "*.xlsx");
        solutionChooser.getExtensionFilters().add(extFilter);
        writeGa.setOnAction(value -> {
            try {
                String path = System.getProperty("user.home") + File.separator + "Documents";
                path += "/GAGUISolution.xlsx";
                solutionFile = new File(path);
                ga.writeToFile(solutionFile);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            File destinationFile = solutionChooser.showSaveDialog(window);
            if (destinationFile != null) {
                try {
                    Files.copy(solutionFile.toPath(), destinationFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    private static void displaySolution(Solution sol) {
        showSolution.setDisable(false);
        int arr[];
        if (!singleFileChoice) {
            arr = new int[10];
        } else {
            arr = new int[20];
        }
        int index = 0;
        for (Project p : sol.getSolutionData().keySet()) {
            Student s = sol.getSolutionData().get(p);
            for (int i = 0; i < s.getPreferenceList().size(); i++) {
                Project ppp = s.getPreferenceList().get(i);
                if (ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())) {
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        showSolution.setOnAction(value -> {
            final Stage myDialog = new Stage();
            myDialog.setTitle("Solution After running GA");
            myDialog.initModality(Modality.WINDOW_MODAL);
            Button okButton = new Button("CLOSE");
            okButton.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent arg0) {
                    myDialog.close();
                }
            });
            CategoryAxis xAxis = new CategoryAxis();
            xAxis.setLabel("Prefrence Assigned");
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Students");
            BarChart barChart = new BarChart(xAxis, yAxis);
            XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.getData().add(new XYChart.Data("1st", arr[0]));
            dataSeries1.getData().add(new XYChart.Data("2nd", arr[1]));
            dataSeries1.getData().add(new XYChart.Data("3rd", arr[2]));
            dataSeries1.getData().add(new XYChart.Data("4th", arr[3]));
            dataSeries1.getData().add(new XYChart.Data("5th", arr[4]));
            dataSeries1.getData().add(new XYChart.Data("6th", arr[5]));
            dataSeries1.getData().add(new XYChart.Data("7th", arr[6]));
            dataSeries1.getData().add(new XYChart.Data("8th", arr[7]));
            dataSeries1.getData().add(new XYChart.Data("9th", arr[8]));
            dataSeries1.getData().add(new XYChart.Data("10th", arr[9]));
            if (singleFileChoice) {
                dataSeries1.getData().add(new XYChart.Data("11th", arr[10]));
                dataSeries1.getData().add(new XYChart.Data("12th", arr[11]));
                dataSeries1.getData().add(new XYChart.Data("13th", arr[12]));
                dataSeries1.getData().add(new XYChart.Data("14th", arr[13]));
                dataSeries1.getData().add(new XYChart.Data("15th", arr[14]));
                dataSeries1.getData().add(new XYChart.Data("16th", arr[15]));
                dataSeries1.getData().add(new XYChart.Data("17th", arr[16]));
                dataSeries1.getData().add(new XYChart.Data("18th", arr[17]));
                dataSeries1.getData().add(new XYChart.Data("19th", arr[18]));
                dataSeries1.getData().add(new XYChart.Data("20th", arr[19]));
            }

            barChart.getData().add(dataSeries1);
            VBox vbox = new VBox(barChart);
            Scene scene = new Scene(vbox, 600, 600);
            myDialog.setScene(scene);
            myDialog.setHeight(600);
            myDialog.setWidth(600);
            myDialog.show();

        });
    }

    private static void displaySolutionBeforeGA(Solution sol) {
        showSolutionBeforeGA.setDisable(false);
        int arr[];
        if (singleFileChoice) {
            arr = new int[20];
        } else {
            arr = new int[10];
        }
        int index = 0;
        for (Project p : sol.getSolutionData().keySet()) {
            Student s = sol.getSolutionData().get(p);
            for (int i = 0; i < s.getPreferenceList().size(); i++) {
                Project ppp = s.getPreferenceList().get(i);
                if (ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())) {
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        showSolutionBeforeGA.setOnAction(value -> {
            final Stage myDialog = new Stage();
            myDialog.setTitle("Solution Before running GA");
            myDialog.initModality(Modality.WINDOW_MODAL);
            Button okButton = new Button("CLOSE");
            okButton.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent arg0) {
                    myDialog.close();
                }
            });
            CategoryAxis xAxis = new CategoryAxis();
            xAxis.setLabel("Prefrence Assigned");
            NumberAxis yAxis = new NumberAxis();
            yAxis.setLabel("Students");
            BarChart barChart = new BarChart(xAxis, yAxis);
            XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.getData().add(new XYChart.Data("1st", arr[0]));
            dataSeries1.getData().add(new XYChart.Data("2nd", arr[1]));
            dataSeries1.getData().add(new XYChart.Data("3rd", arr[2]));
            dataSeries1.getData().add(new XYChart.Data("4th", arr[3]));
            dataSeries1.getData().add(new XYChart.Data("5th", arr[4]));
            dataSeries1.getData().add(new XYChart.Data("6th", arr[5]));
            dataSeries1.getData().add(new XYChart.Data("7th", arr[6]));
            dataSeries1.getData().add(new XYChart.Data("8th", arr[7]));
            dataSeries1.getData().add(new XYChart.Data("9th", arr[8]));
            dataSeries1.getData().add(new XYChart.Data("10th", arr[9]));
            if (singleFileChoice) {
                dataSeries1.getData().add(new XYChart.Data("11th", arr[10]));
                dataSeries1.getData().add(new XYChart.Data("12th", arr[11]));
                dataSeries1.getData().add(new XYChart.Data("13th", arr[12]));
                dataSeries1.getData().add(new XYChart.Data("14th", arr[13]));
                dataSeries1.getData().add(new XYChart.Data("15th", arr[14]));
                dataSeries1.getData().add(new XYChart.Data("16th", arr[15]));
                dataSeries1.getData().add(new XYChart.Data("17th", arr[16]));
                dataSeries1.getData().add(new XYChart.Data("18th", arr[17]));
                dataSeries1.getData().add(new XYChart.Data("19th", arr[18]));
                dataSeries1.getData().add(new XYChart.Data("20th", arr[19]));
            }
            barChart.getData().add(dataSeries1);
            VBox vbox = new VBox(barChart);
            Scene scene = new Scene(vbox, 600, 600);
            myDialog.setScene(scene);
            myDialog.setHeight(600);
            myDialog.setWidth(600);
            myDialog.show();

        });
    }

    private static String isDataValid(Data data) {
        String message = "Correct";
        return message;
    }

    public static File getStudentsFile() {
        return studentsFile;
    }

    public static void setStudentsFile(File studentsFile) {
        studentsFile = studentsFile;
    }

    public static File getProjectsFile() {
        return projectsFile;
    }

    public static void setProjectsFile(File projectsFile) {
        projectsFile = projectsFile;
    }

    public static File getProfessorsFile() {
        return professorsFile;
    }

    public static void setProfessorsFile(File professorsFile) {
        professorsFile = professorsFile;
    }

    public int getDataSetType() {
        return dataSetType;
    }

    public void setDataSetType(int dataSetType) {
        this.dataSetType = dataSetType;
    }

    public static File getSingleFile() {
        return singleFile;
    }

    public static void setSingleFile(File singleFile) {
        singleFile = singleFile;
    }

}


