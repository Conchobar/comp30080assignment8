package com.kovid.a1;

public interface CoolingSchedule {

    //Function that returns a boolean indicating when we should stop the program
    public Boolean stopOrNot();

    //Helper functions
    abstract void reduceTemperature();

    abstract void setIterations(int iterations);

    abstract int getIterations();

    abstract void setTemperature(double temperature);

    abstract double getTemperature();

    abstract void setCoolingRate(double coolingRate);

    abstract String getName();

    abstract void setName(String name);

    abstract double getCoolingRate();
}