package com.kovid.a1;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.*;
import java.util.Random;
public class Solution implements Measurable{

    private HashMap<Project, Student> solutionData;
    private List<Project> allocatedProjects;
    private List<Student> students;
    private HashMap<Project, Student> oldAllocations = new HashMap<Project, Student>();
    private HashMap<Project, Student> newAllocations = new HashMap<Project, Student>();
    private double temperature;
    private double fitness;
    private int lastIteration=0;
    private boolean isValidOffspring=true;

    Solution(){

    }

    //Constructor that creates a soluton generator object, assigns the solution to the class varibale
    Solution(Data data) throws IOException{
        GenerateSolution gs = new GenerateSolution(data.getProjects(), data.getStudents());
        solutionData = new HashMap<Project, Student>(gs.getBestSolution());
        allocatedProjects = gs.getAllocatedProjects();
        students = gs.getStudents();
    }

    //Public function that when called, makes a random change to the solution i.e. changes projects assigned to a some students
    public void randomChange(){
        // System.out.println("getting stuck here S34");
        oldAllocations.clear();
        newAllocations.clear();
        Random rand = new Random();
        int iterations = rand.nextInt(5)+1;
        lastIteration=iterations;
        while(iterations>0){
            // System.out.println("getting stuck here S41");
            boolean checker = true;
            while(checker){
                // System.out.println("getting stuck here S43");
                int size = students.size();
                Random r = new Random();
                boolean check = true;
                int randomIndexOne=0;
                int randomIndexTwo=0;
                while(check){
                    randomIndexOne = r.nextInt(size);
                    randomIndexTwo = r.nextInt(size);
                    if(randomIndexOne!=randomIndexTwo){
                        check = false;
                    }
                }
                Project projectOne = allocatedProjects.get(randomIndexOne);
                Project projectTwo = allocatedProjects.get(randomIndexTwo);
                Student studentOne = solutionData.get(projectOne);
                Student studentTwo = solutionData.get(projectTwo);

                boolean check1=false;
                boolean check2 = false;
                //Checks if the two randomly selected students have their projects in each other's preference lists
                for(Project ppp : studentOne.getPreferenceList()){
                    if(ppp.getTitle().equals(projectTwo.getTitle()) && ppp.getProfessor().equals(projectTwo.getProfessor())){
                        check1 = true;
                    }
                }
                for(Project ppp : studentTwo.getPreferenceList()){
                    if(ppp.getTitle().equals(projectOne.getTitle()) && ppp.getProfessor().equals(projectOne.getProfessor())){
                        check2 = true;
                    }
                }
                if(check1 && check2){
                    //If yes, swap their projects
                    oldAllocations.put(projectOne, studentOne);
                    oldAllocations.put(projectTwo, studentTwo);
                    newAllocations.put(projectOne, studentTwo);
                    newAllocations.put(projectTwo, studentOne);
                    solutionData.put(projectOne, studentTwo);
                    solutionData.put(projectTwo, studentOne);
                    checker = false;
                    iterations--;
                }
            }
        }

    }

    //Function that tells us how many studnets had their projects reassigned as a result of calling randomChange method
    public void numOfRandomChangesMade(){
        if(lastIteration==0){
            System.out.println("No changes have been made to the dataset yet");    
        }
        else{
            System.out.println("\n"+2*lastIteration+" students had their assigned project changed");
        }
    }

    //Function that prints the chnages made to the solution after randomChange() function is called
    public void showChangesMade(){
        if(oldAllocations.isEmpty()){
            System.out.println("No chnages made to the datset yet");
        }
        else{
            System.out.println("\nChanges made to the dataset are -\n");
            for(Project key: oldAllocations.keySet()){
                Student oldStudent = oldAllocations.get(key);
                Student newStudent = newAllocations.get(key);
                System.out.println(key.getTitle()+": This project was first assigned to "+oldStudent.getName()+", and after the random change, it is assigned to student "+newStudent.getName());
            }
            System.out.println();
        }
    }

     //Function that returns the project assigned to a student when the student number is passed as argument
    public Project getStudentsProject(int studentNum){
        Project pro = new Project();
        for (Project p : solutionData.keySet()) {
            Student s = solutionData.get(p);
            if(s.getStudentNumber()==studentNum){
                return p;
            }
        }
        return pro;
    }
    //Function that returns the project assigned to a student when the student's name is passed as argument
    public Project getStudentsProject(String studentName){
        Project pro = new Project();
        String name[] = studentName.split(" ");
        for (Project p : solutionData.keySet()) {
            Student s = solutionData.get(p);
            if(s.getFirstName().equalsIgnoreCase(name[0]) && s.getSurname().equalsIgnoreCase(name[1])){
                return p;
            }
        }
        return pro;
    }

    //Function that when given a project title as argument, returns the student object it is assigned to
    public Student getStudentForProject(String projectTitle){
        Student stu = new Student();
        for(Project p : solutionData.keySet()){
            if(p.getTitle().equalsIgnoreCase(projectTitle)){
                return solutionData.get(p);
            }
        }
        return stu;
    }

    public int getCount() {
        return solutionData.size();
    }

    public HashMap<Project, Student> getSolution(){
        return solutionData;
    }

    public double calculateTemperature(double weight){
        double indexValue = 0;
        double maxIndexValue = 0;
        double gpaValue = 0;
        double maxGPAValue = 0;
        double result = 0;
        double g = weight;

        for(Student student : students){
            Project project = new Project();
            for(Project pro : solutionData.keySet()){
                Student stu = solutionData.get(pro);
                if(stu.getStudentNumber()==student.getStudentNumber()){
                    project = pro;
                    break;
                }
            }

            for(int i = 0;i<student.getPreferenceList().size();i++){
                if(project.getTitle().equals(student.getPreferenceList().get(i).getTitle()) && project.getProfessor().equals(student.getPreferenceList().get(i).getProfessor())){
                    indexValue += (Math.pow(i, 1.2));
                }
            }
            
            maxIndexValue += (Math.pow(10, 1.2));
            gpaValue += (1/(Math.pow(student.getGpa(), 1.1)));
            maxGPAValue += (1/(Math.pow(2.0, 1.1)));
        }
        double indexFormula = (indexValue/maxIndexValue);
        double gpaFormula = (gpaValue/maxGPAValue)*indexFormula;

        result = 100*(((1-g)*(indexFormula)) + (g*gpaFormula));
        return result;
    }

    public double calculateFitness(double weight){
        double fitness = (1/(calculateTemperature(weight)))*100000;

        return fitness;
    }

    public String toString(){
        String str = "";
        int i = 1;

        for (Project p : solutionData.keySet()) {
            Student s = solutionData.get(p);
            str += i + ": " + s.getFullName() + " "+ " Project: " + p.getTitle() + "\n";
            i++;
        }

        return str;
    }
    public HashMap<Project, Student> getSolutionData() {
        return solutionData;
    }

    // public void setSolution(HashMap<Project, Student> newSolution){
    //     solutionData = newSolution;
    // }

    public void setSolutionData(HashMap<Project, Student> solutionData) {
        this.solutionData = solutionData;
    }

    public List<Project> getAllocatedProjects() {
        return allocatedProjects;
    }

    public void setAllocatedProjects(List<Project> allocatedProjects) {
        this.allocatedProjects = allocatedProjects;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public HashMap<Project, Student> getOldAllocations() {
        return oldAllocations;
    }

    public void setOldAllocations(HashMap<Project, Student> oldAllocations) {
        this.oldAllocations = oldAllocations;
    }

    public HashMap<Project, Student> getNewAllocations() {
        return newAllocations;
    }

    public void setNewAllocations(HashMap<Project, Student> newAllocations) {
        this.newAllocations = newAllocations;
    }

    public int getLastIteration() {
        return lastIteration;
    }

    public void setLastIteration(int lastIteration) {
        this.lastIteration = lastIteration;
    }
    public void preferencesAssignedForSingleFile(){
        int arr[]=new int[20];
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            index++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<20;i++){
            System.out.println(arr[i]+" students got their "+(i+1)+" preference");
        }
    }
    public void preferencesAssigned(){
        int arr[]=new int[10];
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            index++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<10;i++){
            System.out.println(arr[i]+" students got their "+(i+1)+" preference");
        }
    }
    public void averagePreferenceForSingleFile(){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        int index=0;
        int arr[]=new int[20];
        int totalPref=0;
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle())){
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<20;i++){
            totalPref+=((i+1)*arr[i]);
        }
        double avg = totalPref;
        avg/=(double)solutionData.size();
        System.out.println("Average preference assigned is "+df.format(avg));
    }
    public void averagePreference(){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        int index=0;
        int arr[]=new int[10];
        int totalPref=0;
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())){
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            index++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<10;i++){
            totalPref+=((i+1)*arr[i]);
        }
        double avg = totalPref;
        avg/=(double)solutionData.size();
        System.out.println("Average preference assigned is "+df.format(avg));
    }
public String returnAveragePreferenceForSingleFile(){
    DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        int index=0;
        int arr[]=new int[20];
        int totalPref=0;
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle())){
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            index++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<20;i++){
            totalPref+=((i+1)*arr[i]);
        }
        double avg = totalPref;
        avg/=(double)solutionData.size();
        return "Average preference assigned is "+df.format(avg)+"\n";
}
    public String returnAveragePreference(){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        int index=0;
        int arr[]=new int[10];
        int totalPref=0;
        for(Project p : solutionData.keySet()){
            Student s = solutionData.get(p);
            for(int i = 0;i<s.getPreferenceList().size();i++){
                Project ppp = s.getPreferenceList().get(i);
                if(ppp.getTitle().equals(p.getTitle()) && ppp.getProfessor().equals(p.getProfessor())){
                    index = i;
                }
            }
            // int index = s.getPreferenceList().indexOf(p);
            arr[index]++;
            index++;
            // System.out.println(s.getName()+" got their "+index+" preference");
        }
        for(int i = 0;i<10;i++){
            totalPref+=((i+1)*arr[i]);
        }
        double avg = totalPref;
        avg/=(double)solutionData.size();
        return "Average preference assigned is "+df.format(avg)+"\n";
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public boolean getIsValidOffspring(){
        return this.isValidOffspring;
    }
    public void setIsValidOffspring(boolean isValidOffspring){
        this.isValidOffspring = isValidOffspring;
    }

}