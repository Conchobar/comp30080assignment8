package com.kovid.a1;

import java.util.Random;

public class MockAcceptance implements AcceptanceFunction{

    private double oldEnergy;
    private double newEnergy;
    private double temperature;
    private String name;

    //Deafult constructor
    public MockAcceptance(){
        this.name = "Mock Acceptance";
    }
    
    //Parameterized constructor to assign values to the instance variables
    public MockAcceptance(double oldEnergy, double newEnergy, double temperature){
        this.oldEnergy = oldEnergy;
        this.newEnergy = newEnergy;
        this.temperature = temperature;
        this.name = "Mock Acceptance";

    }

    //Helper function that returns a value which later helps decide if the chnge should be accpted or not
    public double acceptanceFunction(double oldEnergy, double newEnergy, double temperature){
        double energyDelta = (oldEnergy - newEnergy)*100;
        double result = 1 / (1+((energyDelta)/(temperature)));
        return result;

    }
    
    //Function that returns a boolean indicating if a change should be accepted or not
    public Boolean acceptOrNot(double oldEnergy, double newEnergy, double temperature){
        Random r = new Random();
        if(newEnergy<oldEnergy){
            return true;
        }

        double testDouble = r.nextDouble();
        if((acceptanceFunction(oldEnergy, newEnergy, temperature)) > testDouble){
            return true;
            }
            else{
                return false;
            }

    }

    public double getOldEnergy() {
        return oldEnergy;
    }

    public void setOldEnergy(double oldEnergy) {
        this.oldEnergy = oldEnergy;
    }

    public double getNewEnergy() {
        return newEnergy;
    }

    public void setNewEnergy(double newEnergy) {
        this.newEnergy = newEnergy;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}