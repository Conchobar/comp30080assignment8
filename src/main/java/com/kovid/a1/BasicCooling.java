package com.kovid.a1;

//Class that implements the basic cooling schedule
public class BasicCooling implements CoolingSchedule{

    int numOfTurnsWithoutAcceptedChange;
    String name;

    BasicCooling(){
        this.numOfTurnsWithoutAcceptedChange = 0;
        this.name = "Basic Cooling";

    }

    //A basic cooling method that stops the program if a change hasnt been accepted for a 100 iterations, meaning we are close to a good answer
    public Boolean stopOrNot(){
        if(numOfTurnsWithoutAcceptedChange>999){
            return true;
        }
        else{
            return false;
        }
    }

    public void increaseNumOfTries(){
        numOfTurnsWithoutAcceptedChange++;
    }

    public void resetNumOfTries(){
        numOfTurnsWithoutAcceptedChange=0;
    }

    @Override
    public void setIterations(int iterations) {
    }

    @Override
    public int getIterations() {
        return 0;
    }

    @Override
    public void setTemperature(double temperature) {
    }

    @Override
    public double getTemperature() {
        return 0;
    }

    @Override
    public void setCoolingRate(double coolingRate) {
    }

    @Override
    public double getCoolingRate() {
        return 0;
    }

    public int getNumOfTurnsWithoutAcceptedChange() {
        return numOfTurnsWithoutAcceptedChange;
    }

    public void setNumOfTurnsWithoutAcceptedChange(int numOfTurnsWithoutAcceptedChange) {
        this.numOfTurnsWithoutAcceptedChange = numOfTurnsWithoutAcceptedChange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void reduceTemperature() {
        // TODO Auto-generated method stub

    }
}