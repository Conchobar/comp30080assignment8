package com.kovid.a1;

import java.util.Comparator;


//Class that implements the comparator to sort the solutions list based on fitness
public class FitnessSort implements Comparator<Solution>{

    public int compare(Solution s1, Solution s2){
        if(s1.getFitness() < (s2.getFitness())){
            return 1;
        }
        else if(s1.getFitness() > (s2.getFitness())){
            return -1;
        }
        else 
            return 0;
    }
}