package com.kovid.a1;

import java.io.File;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

@SpringBootApplication
public class A1Application extends Application {

    private Scene chosenAlgorithm, welcomeScene;
    private Stage window;

    // Function that's called when the Application starts
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Kovid fiche fiche");
        welcomeScene = homeScene();
        welcomeScene.getStylesheets().add(this.getClass().getClassLoader().getResource("javaFXstyles.css").toExternalForm());

        System.out.println("Style Sheet: " + welcomeScene.getStylesheets().toString());
        window.setScene(welcomeScene);
        window.show();

    }

    public static void main(String[] args) throws IOException {
        // Launch the application
        SpringApplication.run(A1Application.class, args);
        Application.launch(args);
    }

    // Method that returns the Welcome page Scene
    public Scene homeScene() {

        // Create the welcome page of the application
        Button gaButton = new Button("Genetic Algorithm");
        Button saButton = new Button("Simulated Annealing");
        HBox welcomeBox = new HBox();
        welcomeBox.setPrefWidth(1600);
        welcomeBox.setPrefHeight(900);
        welcomeBox.setMinWidth(800);
        welcomeBox.setMinHeight(600);
        welcomeBox.getChildren().add(gaButton);
        welcomeBox.getChildren().add(saButton);
        gaButton.setMaxWidth(Double.MAX_VALUE);
        saButton.setMaxWidth(Double.MAX_VALUE);
        gaButton.setMaxHeight(Double.MAX_VALUE);
        saButton.setMaxHeight(Double.MAX_VALUE);
        HBox.setHgrow(gaButton, Priority.ALWAYS);
        HBox.setHgrow(saButton, Priority.ALWAYS);
        Scene homeScene = new Scene(welcomeBox, 1280, 720);

        // If the Genetic Algortithm button is clicked, create the GA Scene
        gaButton.setOnAction(value -> {
            GAScene gas = new GAScene(window, welcomeScene);
            chosenAlgorithm = gas.geneticAlgorithmScene();
            chosenAlgorithm.getStylesheets()
                    .add(this.getClass().getClassLoader().getResource("javaFXstyles.css").toExternalForm());
            window.setTitle("Genetic Algorithm");
            window.setScene(chosenAlgorithm);
            window.show();
        });

        // If the Simulated Annealing button is clicked, create the GA Scene
        saButton.setOnAction(value -> {
            SAScene sas = new SAScene(window, welcomeScene);
            chosenAlgorithm = sas.simulatedAnnealingScene();
            chosenAlgorithm.getStylesheets()
                    .add(this.getClass().getClassLoader().getResource("javaFXstyles.css").toExternalForm());
            window.setTitle("Simulated Annealing");
            window.setScene(chosenAlgorithm);
            window.show();
        });

        homeScene.getStylesheets().add("src/main/resources/javaFXstyles.css");
        return homeScene;
    }


    //Method used by test classes
    public static Scene homeSceneTester() {
        

        // Create the welcome page of the application
        Button gaButtonTest = new Button("Genetic Algorithm");
        gaButtonTest.setId("GA");
        Button saButtonTest = new Button("Simulated Annealing");
        saButtonTest.setId("SA");
        HBox welcomeBoxTest = new HBox();
        welcomeBoxTest.setPrefWidth(1600);
        welcomeBoxTest.setPrefHeight(900);
        welcomeBoxTest.setMinWidth(800);
        welcomeBoxTest.setMinHeight(600);
        welcomeBoxTest.getChildren().add(gaButtonTest);
        welcomeBoxTest.getChildren().add(saButtonTest);
        gaButtonTest.setMaxWidth(Double.MAX_VALUE);
        saButtonTest.setMaxWidth(Double.MAX_VALUE);
        gaButtonTest.setMaxHeight(Double.MAX_VALUE);
        saButtonTest.setMaxHeight(Double.MAX_VALUE);
        HBox.setHgrow(gaButtonTest, Priority.ALWAYS);
        HBox.setHgrow(saButtonTest, Priority.ALWAYS);
        Scene homeSceneTest = new Scene(welcomeBoxTest, 1280, 720);

        Stage windowTest = new Stage();
        // If the Genetic Algortithm button is clicked, create the GA Scene
        gaButtonTest.setOnAction(value -> {
            gaButtonTest.setText("Clicked");
            GAScene gas = new GAScene(windowTest, homeSceneTest);
            Scene chosenAlgorithmTest = gas.geneticAlgorithmScene();
            windowTest.setTitle("Genetic Algorithm");
            windowTest.setScene(chosenAlgorithmTest);
            windowTest.show();
        });

        //If the Simulated Annealing button is clicked, create the GA Scene
        saButtonTest.setOnAction(value ->  {
            saButtonTest.setText("Clicked");
            SAScene sas = new SAScene(windowTest, homeSceneTest);
            Scene chosenAlgorithmTest = sas.simulatedAnnealingScene();
            windowTest.setTitle("Simulated Annealing");
            windowTest.setScene(chosenAlgorithmTest);
            windowTest.show();
        });

        return homeSceneTest;
    }


    public Scene getChosenAlgorithm() {
        return chosenAlgorithm;
    }

    public void setChosenAlgorithm(Scene chosenAlgorithm) {
        this.chosenAlgorithm = chosenAlgorithm;
    }

    public Stage getWindow() {
        return window;
    }

    public void setWindow(Stage window) {
        this.window = window;
    }


}


