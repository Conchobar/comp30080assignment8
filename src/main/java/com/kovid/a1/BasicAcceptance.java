package com.kovid.a1;

import java.util.Random;

public class BasicAcceptance implements AcceptanceFunction{

    private double maxTemperature;
    private String name;

    public BasicAcceptance(){
        this.name = "Basic Acceptance";
        this.maxTemperature = 100;
    }

    //Constructor that sets the maxTempearture value when the class object is created
    public BasicAcceptance(double maxTemperature){
        this.maxTemperature = maxTemperature;
        this.name = "Basic Acceptance";

    }

    //Helper function that returns a value which later helps decide if the chnge should be accpted or not
    @Override
    public double acceptanceFunction(double energy1, double energy2, double temperature) {
        return temperature/maxTemperature;
    }

    @Override
    //Function that returns a boolean indicating if a change should be accepted or not
    public Boolean acceptOrNot(double energy1, double energy2, double temperature) {
        if(energy2<energy1){
            return true;
        }

        Random r = new Random();
        double rand = r.nextDouble();
        if((acceptanceFunction(energy1, energy2, temperature))>rand){
            return true;
        }
        else{
            return false;
        }
    }

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}