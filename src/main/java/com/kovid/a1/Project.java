package com.kovid.a1;


public class Project{
    private String professor;
    private String title;
    private String stream;
    
    public Project(){
        this.professor = "";
        this.title = "";
    }

    public Project(String professor, String title, String school){
        this.professor = professor;
        this.title = title;

        if(school.equals("DS")){
            this.stream = "DS";
        }
        else if(school.equals("CS")){
            this.stream = "CS";
        }
        else{
            this.stream = "CS + DS";
        }
    }

    public String toString(){
        String str="";
        str = str+title+ " - ";
        
        return str;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProjectStream() {
        return stream;
    }

    public void setProjectStream(String stream) {
        this.stream = stream;
    }
}