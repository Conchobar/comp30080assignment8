package com.kovid.a1;
import java.util.*;

public class Professor {
    private String name;
    private String interest;
    private List<Project> projects = new ArrayList<Project>();
    private Boolean isDS = true;

    public String toString(){
        String str ="Name: "+name+" - Research Interests: "+interest+" - Projects - ";
        for(int i=0;i<projects.size();i++){
            str+=projects.get(i).toString()+" ";
        }
        return str;
    }
    
    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setProjects(List<Project> projects){
        this.projects = projects;
    }

    public List<Project> getProjects(){
        return this.projects;
    }

    public void setIsDS(Boolean isDS){
        this.isDS = isDS;
    }

    public Boolean getIsDS(){
        return this.isDS;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}