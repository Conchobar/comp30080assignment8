package com.kovid.a1;

//Class that implements the decreasing rate cooling schedule
public class DecreasingRateCooling implements CoolingSchedule {

    private double maxTemperature;
    private double temperature;
    private double coolingRate;
    private int iterations;
    private String name;

    public DecreasingRateCooling(){
        this.maxTemperature = 1000;
        this.temperature = maxTemperature;
        this.coolingRate = 0;
        this.iterations = 100;
        this.name = "Decreasing Rate Cooling";
    }

    public DecreasingRateCooling(double temperature, double coolingRate){
        this.maxTemperature = temperature;
        this.temperature = temperature;
        this.coolingRate = coolingRate;
        this.name = "Decreasing Rate Cooling";

    }

    public Boolean stopOrNot(){
        if(temperature <= 0.1){
            temperature = maxTemperature;
            return true;
        }
        else{
            return false;
        }
    }

    public void reduceTemperature(){

        temperature*=0.75;
        iterations=0;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getCoolingRate() {
        return coolingRate;
    }

    public void setCoolingRate(double coolingRate) {
        this.coolingRate = coolingRate;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}