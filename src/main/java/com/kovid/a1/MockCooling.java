package com.kovid.a1;

public class MockCooling implements CoolingSchedule{
    private double maxTemperature;
    private double temperature;
    private double coolingRate;
    private int iterations;
    private String name;

    //Default constructor
    public MockCooling(){
        this.maxTemperature = 100;
        this.temperature = maxTemperature;
        this.coolingRate = 10;
    }

    //Parameterized constructor that assigns values to the insatcne variables
    public MockCooling(double temperature, double coolingRate){
        this.temperature = temperature;
        this.coolingRate = coolingRate;
        this.name = "Mock Cooling";
    }

    //Function that returns a boolean indicating if we should stop the prorgam or not
    public Boolean stopOrNot(){
        if(temperature <= 0){
            temperature = maxTemperature;
            return true;
        }
        else{
            return false;
        }
    }

    public void reduceTemperature(){

        temperature-=coolingRate;
        iterations=0;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getCoolingRate() {
        return coolingRate;
    }

    public void setCoolingRate(double coolingRate) {
        this.coolingRate = coolingRate;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}