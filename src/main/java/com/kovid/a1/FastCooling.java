package com.kovid.a1;

//Class that implements the fastcooling schedule
public class FastCooling implements CoolingSchedule{

    private double maxTemperature;
    private double temperature;
    private double coolingRate;
    private int iterations;
    private String name;

    //Default constructor
    public FastCooling(){
        this.maxTemperature = 100;
        this.temperature = maxTemperature;
        this.coolingRate = 1;
        this.iterations = 10;
        this.name = "Fast Cooling";
    }

    //Constrcutor to initialize instance varibales
    public FastCooling(double temperature, double coolingRate){
        this.temperature = temperature;
        this.coolingRate = coolingRate;
        this.name = "Fast Cooling";

    }

    //Function that returns a boolean indicating when we should stop the program
    public Boolean stopOrNot(){
        if(temperature <= 0){
            temperature = maxTemperature;
            return true;
        }
        else{
            return false;
        }
    }
    
    public void reduceTemperature(){

        temperature-=coolingRate;
        iterations=0;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getCoolingRate() {
        return coolingRate;
    }

    public void setCoolingRate(double coolingRate) {
        this.coolingRate = coolingRate;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}