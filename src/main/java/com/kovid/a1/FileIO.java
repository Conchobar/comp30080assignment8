package com.kovid.a1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.application.Platform;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class FileIO {

    private static List<String> gpaPermutations = new ArrayList<String>();
    private static List<String> studentNamePermutations = new ArrayList<String>();
    private static List<String> studentNumberPermutations = new ArrayList<String>();
    private static List<String> proposerPermutations = new ArrayList<String>();
    private static List<String> titlePermutations = new ArrayList<String>();
    private static List<String> professorPermutations = new ArrayList<String>();
    private static List<String> researchInterestsPermutations = new ArrayList<String>();
    private static List<String> streamPermutations = new ArrayList<String>();
    private static List<String> namePermutations = new ArrayList<String>();

    //Method that takes the VBox and the solution as arguyments and prints it on the screen
    public static void printSolution(Solution solution, VBox output, String edits, boolean singleFile) {
        VBox names = new VBox();
        VBox projects = new VBox();
        HBox colourisedResults = new HBox(names, projects);
        Text editsText = new Text(edits);
        ScrollPane sp = (ScrollPane) output.getChildren().get(1);
        TextFlow tf = (TextFlow) sp.getContent();
        String stri = "";
        if (!singleFile) {
            stri = solution.returnAveragePreference() + "\n";
        } else {
            stri = solution.returnAveragePreferenceForSingleFile() + "\n";
        }
        double d1, d2;
        if (singleFile) {
            d1 = 0.2;
            d2 = 0.6;
        } else {
            d1 = 0.4;
            d2 = 0.7;
        }
        //Take a student's assigned project, find its index relative to its preference list index and assign GREEN/ORANGE/RED
        //based on its position.
        for (Map.Entry<Project, Student> entry : solution.getSolutionData().entrySet()) {
            for (int i = 0; i < entry.getValue().getPreferenceList().size(); i++) {
                if (entry.getKey().getTitle().equals(entry.getValue().getPreferenceList().get(i).getTitle())) {
                    if (i < entry.getValue().getPreferenceList().size() * d1) {
                        String studentString = "";
                        if (!singleFile) {
                            studentString = "Name: " + entry.getValue().getFirstName() + " "
                                    + entry.getValue().getSurname();
                        } else {
                            studentString = "Name: " + entry.getValue().getFullName();
                        }
                        String projectString = "Project: " + entry.getKey().getTitle();
                        Text studentText = new Text(studentString);
                        studentText.setTextAlignment(TextAlignment.LEFT);
                        studentText.setFill(Color.GREEN);
                        Text projectText = new Text(projectString);
                        projectText.setTextAlignment(TextAlignment.LEFT);
                        projectText.setFill(Color.GREEN);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                names.getChildren().add(studentText);
                                projects.getChildren().add(projectText);
                            }
                        });
                    } else if (i >= entry.getValue().getPreferenceList().size() * d1
                            && i <= entry.getValue().getPreferenceList().size() * d2) {
                        String studentString = "";
                        if (!singleFile) {
                            studentString = "Name: " + entry.getValue().getFirstName() + " "
                                    + entry.getValue().getSurname();
                        } else {
                            studentString = "Name: " + entry.getValue().getFullName();
                        }
                        String projectString = "Project: " + entry.getKey().getTitle();
                        Text studentText = new Text(studentString);
                        studentText.setTextAlignment(TextAlignment.LEFT);
                        studentText.setFill(Color.ORANGE);
                        Text projectText = new Text(projectString);
                        projectText.setTextAlignment(TextAlignment.LEFT);
                        projectText.setFill(Color.ORANGE);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                names.getChildren().add(studentText);
                                projects.getChildren().add(projectText);
                            }
                        });
                    } else {
                        String studentString = "";
                        if (!singleFile) {
                            studentString = "Name: " + entry.getValue().getFirstName() + " "
                                    + entry.getValue().getSurname();
                        } else {
                            studentString = "Name: " + entry.getValue().getFullName();
                        }
                        String projectString = "Project: " + entry.getKey().getTitle();
                        Text studentText = new Text(studentString);
                        studentText.setTextAlignment(TextAlignment.LEFT);
                        studentText.setFill(Color.RED);
                        Text projectText = new Text(projectString);
                        projectText.setTextAlignment(TextAlignment.LEFT);
                        projectText.setFill(Color.RED);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                names.getChildren().add(studentText);
                                projects.getChildren().add(projectText);
                            }
                        });
                    }
                }
            }
        }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                tf.getChildren().add(colourisedResults);
            }
        });

        //Only expecting edits in single File scenario
        if(singleFile){
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    tf.getChildren().add(editsText);
                }
            });
        }

        System.out.println("Edits\n\n" + edits);
    }

    //Single file validator
    public static String isFileValid(File file) throws IOException, ParseException {
        
        String message = "Correct";
        loadPermutations();

        //Checks for correct file extension
        String fileName = file.getName();
        String fileExtension = fileName.substring((fileName.length()-5), fileName.length());
        System.out.println("Chars: " + (fileName.substring((fileName.length()-5), fileName.length())));
        if(!(fileExtension.equals(".xlsx"))){
            message = "Error: .xlsx file format required, please select a suitable File.";
            return message;
        }
            FileInputStream fis = new FileInputStream(file);          
    
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
    
            XSSFSheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                message = "Error: Could not find file";
                throw new ParseException("No sheet found on index 0", 1);
            }
            //set gpa index to zero as it may not exist
            int studentCell=-0,studentNumCell=-0,gpaCell=-1,proposerCell=-0;
            Row headerRow = mySheet.getRow(0);
            int cellCount=0;
            Cell student, stuNumber, gpa, proposer;
            Cell firstCell = headerRow.getCell(0);

            //Search for column headers
            if(firstCell.getCellType()==Cell.CELL_TYPE_STRING){
                String str = firstCell.getStringCellValue();
                str=str.trim();
                if(studentNamePermutations.contains(str)){
                    studentCell=cellCount;
                    cellCount++;
                }     
                if(studentNumberPermutations.contains(str)){
                    studentNumCell=cellCount;
                    cellCount++;
                }
                if(gpaPermutations.contains(str)){
                    gpaCell=cellCount;
                    cellCount++;
                }
                if(proposerPermutations.contains(str)){
                    proposerCell=cellCount;
                    cellCount++;
                }
            }
            else{
                cellCount++;
            }
            System.out.println("Cellcount before iterator: " + cellCount);
            Iterator<Cell> cellIterator = headerRow.cellIterator();
            Cell cellN = cellIterator.next();
            while(cellIterator.hasNext()){
                cellN = cellIterator.next();
                if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                    if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                        continue;
                    } 
                    String str = cellN.getStringCellValue();
                    str=str.trim();
                    if(studentNamePermutations.contains(str)){
                        studentCell=cellCount;
                        cellCount++;
                    }     
                    if(studentNumberPermutations.contains(str)){
                        studentNumCell=cellCount;
                        cellCount++;
                    }
                    if(gpaPermutations.contains(str)){
                        gpaCell=cellCount;
                        cellCount++;
                    }
                    if(proposerPermutations.contains(str)){
                        proposerCell=cellCount;
                        cellCount++;
                    }
                }
                else{
                    cellCount++;
                }
            }
            
            
            String gpaString=""; 
            student = headerRow.getCell(studentCell);
            stuNumber = headerRow.getCell(studentNumCell);
            //Needs fix
            System.out.println("gpaCell is: " + gpaCell + " before if statement");

            if(gpaCell!= -1){
                gpa = headerRow.getCell(gpaCell);
                gpaString = gpa.getStringCellValue();
            }
            proposer = headerRow.getCell(proposerCell);


            String studentString = student.getStringCellValue();
            String studentNumString = stuNumber.getStringCellValue();
            String proposerString = proposer.getStringCellValue();
            System.out.println("Studentstr: " + studentString + "\nNumberstr: " + studentNumString + "\nGPAstr: " + gpaString + "\nProposerstr: " + proposerString);

            //Update error message if columns missing
            if(!((studentNamePermutations.contains(studentString))&&(studentNumberPermutations.contains(studentNumString))
                &&(gpaPermutations.contains(gpaString))&&(proposerPermutations.contains(proposerString)))){
                    String colError = "";
                if(!(studentNamePermutations.contains(studentString))){
                    colError += studentString + " ";
                }
                if(!(studentNumberPermutations.contains(studentNumString))){
                    colError += studentNumString + " ";
                }
                if((!(gpaPermutations.contains(gpaString)))){
                    colError += gpaString + " ";
                }
                if(!(proposerPermutations.contains(proposerString))){
                    colError += proposerString + " ";
                }

                message = "Error: Incorrect format for column headers detected.";
                message += "\nPlease ensure File is of correct format before proceeding.";
                message += "\n" + colError + "not recognised";
            }

            //More than 20 entries + gpa/student/number/proposer encountered
            if(!(headerRow.getCell(24) == null)){
                message = "Error: More entries in file columns than expected.";
                message += "\nPlease ensure File is of correct format before proceeding.";

            }

        workbook.close();
        return message;
    }
    
        //Works like isFileValid, but for 3 distinct files
        public static String threeFilesValid(File studentFile, File projectFile, File professorFile) throws IOException, ParseException {
            
            String message = "Correct";
            loadPermutations();
    
            String studentFileName = studentFile.getName();
            String studentFileExtension = studentFileName.substring((studentFileName.length()-5), studentFileName.length());
            System.out.println("Chars: " + (studentFileName.substring((studentFileName.length()-5), studentFileName.length())));
            if(!(studentFileExtension.equals(".xlsx"))){
                message = "Error: .xlsx file format required fro Student File, please select a suitable File.";
                return message;
            }

            String projectFileName = projectFile.getName();
            String projectFileExtension = projectFileName.substring((projectFileName.length()-5), projectFileName.length());
            System.out.println("Chars: " + (projectFileName.substring((projectFileName.length()-5), projectFileName.length())));
            if(!(projectFileExtension.equals(".xlsx"))){
                message = "Error: .xlsx file format required fro Project File, please select a suitable File.";
                return message;
            }

            String professorFileName = professorFile.getName();
            String professorFileExtension = professorFileName.substring((professorFileName.length()-5), professorFileName.length());
            System.out.println("Chars: " + (professorFileName.substring((professorFileName.length()-5), professorFileName.length())));
            if(!(professorFileExtension.equals(".xlsx"))){
                message = "Error: .xlsx file format required fro Professor File, please select a suitable File.";
                return message;
            }


            FileInputStream stuFis = new FileInputStream(studentFile);          
            HSSFWorkbook workbook = new HSSFWorkbook(stuFis);
    
            HSSFSheet mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                message = "Error: Could not find file";
                throw new ParseException("No sheet found on index 0", 1);
            }

            int studentRows = mySheet.getPhysicalNumberOfRows() - 1;
            int studentCell = 0, studentNumberCell = 0, gpaCell = -1, streamCell = 0;
            Row headerRow = mySheet.getRow(0);
            int cellCount = 0;
            Cell name, stuNumber, stream, gpa ;
            Cell firstCell = headerRow.getCell(0);

            if(firstCell.getCellType()==Cell.CELL_TYPE_STRING){
                String str = firstCell.getStringCellValue();
                str=str.trim();
                if(studentNamePermutations.contains(str)){
                    studentCell=cellCount;
                    cellCount++;
                }     
                if(studentNumberPermutations.contains(str)){
                    studentNumberCell=cellCount;
                    cellCount++;
                }
                if(gpaPermutations.contains(str)){
                    gpaCell=cellCount;
                    cellCount++;
                }
                if(streamPermutations.contains(str)){
                    streamCell=cellCount;
                    cellCount++;
                }
            }
            else{
                cellCount++;
            }

            Iterator<Cell> cellIterator = headerRow.cellIterator();
            Cell cellN = cellIterator.next();
            while(cellIterator.hasNext()){
                cellN = cellIterator.next();
                if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                    if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                        continue;
                    } 
                    String str = cellN.getStringCellValue();
                    str=str.trim();
                    if(studentNamePermutations.contains(str)){
                        studentCell=cellCount;
                        cellCount++;
                    }     
                    if(studentNumberPermutations.contains(str)){
                        studentNumberCell=cellCount;
                        cellCount++;
                    }
                    if(gpaPermutations.contains(str)){
                        gpaCell=cellCount;
                        cellCount++;
                    }
                    if(streamPermutations.contains(str)){
                        streamCell=cellCount;
                        cellCount++;
                    }
                }
                else{
                    cellCount++;
                }
            }

            String gpaString = "";
            name = headerRow.getCell(studentCell);
            stuNumber = headerRow.getCell(studentNumberCell);
            //Needs fix
            System.out.println("gpaCell is: " + gpaCell + " before if statement");
            if(gpaCell!= -1){
                gpa = headerRow.getCell(gpaCell);
                gpaString = gpa.getStringCellValue();
            }
            stream = headerRow.getCell(streamCell);

            String studentString = name.getStringCellValue();
            String studentNumString = stuNumber.getStringCellValue();
            String streamString = stream.getStringCellValue();
            String colError = "";

            if(!((studentNamePermutations.contains(studentString))&&(studentNumberPermutations.contains(studentNumString))
            &&(gpaPermutations.contains(gpaString))&&(streamPermutations.contains(streamString)))){
                if(!(studentNamePermutations.contains(studentString))){
                    colError += studentString + " ";
                }
                if(!(studentNumberPermutations.contains(studentNumString))){
                    colError += studentNumString + " ";
                }
                if(!((gpaPermutations.contains(gpaString))&&(!(gpaString.isEmpty())))){
                    colError += gpaString + " ";
                }
                if(!(streamPermutations.contains(streamString))){
                    colError += streamString + " ";
                }

            // if(!((studentString.equals("Student")) && (studentNumString.equals("Student Number")) 
            //     && (gpaString.equals("GPA") || gpaString.equals(" ")) && (proposerString.equals("Proposer")))){
                message = "Error: Incorrect format for column headers in Student File detected.";
                message += "\nPlease ensure File is of correct format before proceeding.";
                message += "\n" + "Column \" " + colError + "\" not recognised";
            }

            if(!(headerRow.getCell(14) == null)){
                message = "Error: More entries in file columns of Student File than expected.";
                message += "\nPlease ensure File is of correct format before proceeding.";
                workbook.close();
                return message;
            }

            workbook.close();

            FileInputStream projFis = new FileInputStream(projectFile);          
            workbook = new HSSFWorkbook(projFis);

            mySheet = workbook.getSheetAt(0);
            if (mySheet == null) {
                workbook.close();
                message = "Error: Could not find file";
                throw new ParseException("No sheet found on index 0", 1);
            }

            int projectRows = mySheet.getPhysicalNumberOfRows() - 1;

            int titleCell = 0, professorCell = 0;
            streamCell = 0;
            headerRow = mySheet.getRow(0);
            cellCount = 0;
            Cell title, professor;//also use Cell stream
            firstCell = headerRow.getCell(0);

            if(firstCell.getCellType()==Cell.CELL_TYPE_STRING){
                String str = firstCell.getStringCellValue();
                str=str.trim();
                if(titlePermutations.contains(str)){
                    titleCell=cellCount;
                    cellCount++;
                }     
                if(professorPermutations.contains(str)){
                    professorCell=cellCount;
                    cellCount++;
                }
                if(streamPermutations.contains(str)){
                    streamCell=cellCount;
                    cellCount++;
                }
            }
            else{
                cellCount++;
            }

            cellIterator = headerRow.cellIterator();
            cellN = cellIterator.next();
            while(cellIterator.hasNext()){
                cellN = cellIterator.next();
                if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                    if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                        continue;
                    } 
                    String str = cellN.getStringCellValue();
                    str=str.trim();
                    if(titlePermutations.contains(str)){
                        titleCell=cellCount;
                        cellCount++;
                    }     
                    if(professorPermutations.contains(str)){
                        professorCell=cellCount;
                        cellCount++;
                    }
                    if(streamPermutations.contains(str)){
                        streamCell=cellCount;
                        cellCount++;
                    }
                }
                else{
                    cellCount++;
                }
            }

            title = headerRow.getCell(titleCell);
            professor = headerRow.getCell(professorCell);
            stream = headerRow.getCell(streamCell);

            String titleString = title.getStringCellValue();
            String professorString = professor.getStringCellValue();
            streamString = stream.getStringCellValue();

            colError = "";
            if(!((titlePermutations.contains(titleString))&&(professorPermutations.contains(professorString))
            &&(streamPermutations.contains(streamString)))){
                if(!(professorPermutations.contains(professorString))){
                    colError += professorString + " ";
                }
                if(!(titlePermutations.contains(titleString))){
                    colError += titleString + " ";
                }
                if(!(streamPermutations.contains(streamString))){
                    colError += streamString + " ";
                }

                message = "Error: Incorrect format for column headers in Project File detected.";
                message += "\nPlease ensure File is of correct format before proceeding.";
                message += "\n" + "Column \" " + colError + "\" not recognised";

                workbook.close();
                return message;
            }

        if(!(headerRow.getCell(14) == null)){
            message = "Error: More entries in file columns of Project File than expected.";
            message += "\nPlease ensure File is of correct format before proceeding.";
            workbook.close();
            return message;
        }

        workbook.close();



        FileInputStream profFis = new FileInputStream(professorFile);          
        workbook = new HSSFWorkbook(profFis);

        mySheet = workbook.getSheetAt(0);
        if (mySheet == null) {
            workbook.close();
            message = "Error: Could not find file";
            throw new ParseException("No sheet found on index 0", 1);
        }

        int professorRows = mySheet.getPhysicalNumberOfRows() - 1;

        professorCell = 0;
        int researchInterestsCell = 0;//Also use nameCell
        headerRow = mySheet.getRow(0);
        Cell researchInterests;//Also use Cell professor
        firstCell = headerRow.getCell(0);
        cellCount = 0;

        if(firstCell.getCellType()==Cell.CELL_TYPE_STRING){
            String str = firstCell.getStringCellValue();
            str=str.trim();
            if(namePermutations.contains(str)){
                professorCell=cellCount;
                System.out.println("professorCell assigned index: " + professorCell);
                cellCount++;
            }     
            if(researchInterestsPermutations.contains(str)){
                researchInterestsCell=cellCount;
                System.out.println("researchInterestsCell assigned index: " + researchInterestsCell);
                cellCount++;
            }
        }
        else{
            cellCount++;
        }

        cellIterator = headerRow.cellIterator();
        cellN = cellIterator.next();
        while(cellIterator.hasNext()){
            cellN = cellIterator.next();
            if(cellN.getCellType()==Cell.CELL_TYPE_STRING){
                if(cellN.getStringCellValue()==null || cellN.getStringCellValue().equals("") || cellN.getStringCellValue().equals(" ")){
                    continue;
                } 
                String str = cellN.getStringCellValue();
                str=str.trim();
                if(namePermutations.contains(str)){
                    professorCell=cellCount;
                    System.out.println("professorCell assigned index: " + professorCell);

                    cellCount++;
                }     
                if(researchInterestsPermutations.contains(str)){
                    researchInterestsCell=cellCount;
                    System.out.println("researchInterestsCell assigned index: " + researchInterestsCell);
                    cellCount++;
                }
            }
            else{
                cellCount++;
            }
        }

        professor = headerRow.getCell(professorCell);
        researchInterests = headerRow.getCell(researchInterestsCell);

        String nameString = professor.getStringCellValue();
        String researchInterestsString = researchInterests.getStringCellValue();
    
        colError = "";
        if(!((researchInterestsPermutations.contains(researchInterestsString))
        &&(namePermutations.contains(nameString)))){
            if(!(researchInterestsPermutations.contains(researchInterestsString))){
                colError += researchInterestsString + " ";
            }
            if(!(namePermutations.contains(nameString))){
                colError += nameString + " ";
            }

            message = "Error: Incorrect format for column headers in Professor File detected.";
            message += "\nPlease ensure File is of correct format before proceeding.";
            message += "\n" + "Column \" " + colError + "\" not recognised";

            workbook.close();
            return message;
        }

        if((studentRows<((professorRows*2)*.75)) || (studentRows>((professorRows*2)*1.3))){
            message = "Ratio of Students to Professors does not fall within acceptable range.";
            message += "\nPlease check that these files are consistent with each other.";
            workbook.close();
            return message;
        }

        if((projectRows<((studentRows*1.5)*.75)) || ((projectRows>((studentRows*1.5)*1.25)))){
            message = "Ratio of Students to Projects does not fall within acceptable range.";
            message += "\nPlease check that these files are consistent with each other.";
            workbook.close();
            return message;
 
        }

        workbook.close();
        return message;
    }

    //Loads the lists with permutations of expected header titles in chosen excel files
    private static void loadPermutations(){

        studentNamePermutations.add("Student");
        studentNamePermutations.add("student");
        studentNamePermutations.add("student names");
        studentNamePermutations.add("Student Names");
        studentNamePermutations.add("Student names");
        studentNamePermutations.add("student Names");
        studentNamePermutations.add("student's names");
        studentNamePermutations.add("Student's Names");
        studentNamePermutations.add("student's Names");
        studentNamePermutations.add("studentnames");
        studentNamePermutations.add("StudentNames");
        studentNamePermutations.add("names");
        studentNamePermutations.add("Names");
        studentNamePermutations.add("Names");
        studentNamePermutations.add("student name");
        studentNamePermutations.add("Student Name");
        studentNamePermutations.add("Student name");
        studentNamePermutations.add("student Name");
        studentNamePermutations.add("student's name");
        studentNamePermutations.add("Student's Name");
        studentNamePermutations.add("student's Name");
        studentNamePermutations.add("studentname");
        studentNamePermutations.add("StudentName");
        studentNamePermutations.add("name");
        studentNamePermutations.add("Name");

        gpaPermutations.add("gpa");
        gpaPermutations.add("GPA");
        gpaPermutations.add("Gpa");
        gpaPermutations.add("grade point average");
        gpaPermutations.add("Grade Point Average");
        gpaPermutations.add(" ");
        gpaPermutations.add("");


        studentNumberPermutations.add("Student Number");
        studentNumberPermutations.add("student Number");
        studentNumberPermutations.add("Student number");
        studentNumberPermutations.add("StudentNumber");
        studentNumberPermutations.add("Studentnumber");
        studentNumberPermutations.add("studentnumber");
        studentNumberPermutations.add("Student's Number");
        studentNumberPermutations.add("Student'sNumber");
        studentNumberPermutations.add("student'snumber");
        studentNumberPermutations.add("Student Numbers");
        studentNumberPermutations.add("student Numbers");
        studentNumberPermutations.add("Student numbers");
        studentNumberPermutations.add("StudentNumbers");
        studentNumberPermutations.add("studentnumbers");
        studentNumberPermutations.add("Student's Numbers");
        studentNumberPermutations.add("Student'sNumbers");
        studentNumberPermutations.add("student'snumbers");
        studentNumberPermutations.add("number");
        studentNumberPermutations.add("numbers");
        studentNumberPermutations.add("Number");
        studentNumberPermutations.add("Numbers");



        proposerPermutations.add("Proposer");
        proposerPermutations.add("proposer");
        proposerPermutations.add("Proposers");
        proposerPermutations.add("proposers");
        proposerPermutations.add("Proposed By");
        proposerPermutations.add("proposed by");

        //title, professor, research interests
        titlePermutations.add("Title");
        titlePermutations.add("Titles");
        titlePermutations.add("title");
        titlePermutations.add("titles");

        professorPermutations.add("Professor");
        professorPermutations.add("Professors");
        professorPermutations.add("Proffessor");
        professorPermutations.add("Proffessors");
        professorPermutations.add("professor");
        professorPermutations.add("professors");
        professorPermutations.add("proffessor");
        professorPermutations.add("proffessors");

        researchInterestsPermutations.add("Research Interest");
        researchInterestsPermutations.add("Research interest");
        researchInterestsPermutations.add("research Interest");
        researchInterestsPermutations.add("research interest");
        researchInterestsPermutations.add("ResearchInterest");
        researchInterestsPermutations.add("researchinterest");
        researchInterestsPermutations.add("Research Interests");
        researchInterestsPermutations.add("Research interests");
        researchInterestsPermutations.add("research Interests");
        researchInterestsPermutations.add("research interests");
        researchInterestsPermutations.add("ResearchInterests");
        researchInterestsPermutations.add("researchinterests");

        streamPermutations.add("Stream");
        streamPermutations.add("Streams");
        streamPermutations.add("stream");
        streamPermutations.add("streams");

        namePermutations.add("name");
        namePermutations.add("Name");
        namePermutations.add("names");
        namePermutations.add("Names");









        


    }
}