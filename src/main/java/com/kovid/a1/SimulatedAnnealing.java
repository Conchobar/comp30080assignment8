package com.kovid.a1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javafx.application.Platform;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;


public class SimulatedAnnealing {
 
    private Solution solution;
    private AcceptanceFunction acceptanceFunction;
    private CoolingSchedule coolingSchedule;
    private double gpaWeight;
    private Solution bestSolution;
    private String edits;
    private boolean singleFilechoice;

    //Constrcutor to initialize instance variables of the class
    SimulatedAnnealing(boolean singleFileChoice, String edits, Solution solution, AcceptanceFunction acceptanceFunction, CoolingSchedule coolingSchedule, double gpaWeight){
        this.solution = solution;
        this.singleFilechoice = singleFileChoice;
        this.edits = edits;
        this.acceptanceFunction = acceptanceFunction;
        this.coolingSchedule = coolingSchedule;
        this.gpaWeight = gpaWeight;

    }

    SimulatedAnnealing(boolean singleFileChoice, String edits, Solution solution, String acceptanceFunction, String coolingSchedule, double gpaWeight){
        this.solution = solution;
        this.singleFilechoice = singleFileChoice;
        this.edits = edits;
        this.acceptanceFunction = textConvertAcceptanceFunction(acceptanceFunction);
        this.coolingSchedule = textConvertCoolingScedule(coolingSchedule);
        this.gpaWeight = gpaWeight;
    }
    

    // Function that implements hillclimbing based on the acceptance function and
    // cooling schedule initialized by the constructor
    public void hillClimbing(){
        double oldEnergy = solution.calculateTemperature(gpaWeight);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        
        while(!coolingSchedule.stopOrNot()){
            coolingSchedule.setIterations(solution.getCount());
            while(coolingSchedule.getIterations()>0){
                HashMap<Project, Student> copySolution = new HashMap<Project, Student>(solution.getSolution());
                solution.randomChange();
                double newEnergy = solution.calculateTemperature(gpaWeight);
                coolingSchedule.setIterations(coolingSchedule.getIterations()-1);
                if(acceptanceFunction.acceptOrNot(oldEnergy, newEnergy, coolingSchedule.getTemperature())){
                    oldEnergy = newEnergy;
                }
                else{
                    solution.setSolutionData(copySolution);
                }
            }
            // System.out.println("ENERGY AT TEMPERATURE " + df.format(coolingSchedule.getTemperature()) + ": " + df.format(solution.calculateTemperature(gpaWeight)));
            coolingSchedule.reduceTemperature();
        }
        this.setBestSolution(solution);
    }

    private CoolingSchedule textConvertCoolingScedule(String coolingScheduleName) {
        CoolingSchedule cs = new BasicCooling();

        if(coolingScheduleName.equals("Basic Cooling")){
            cs = new BasicCooling();
        }
        else if(coolingScheduleName.equals("Fast Cooling")){
            cs = new FastCooling();
        }
        else if(coolingScheduleName.equals("Slow Cooling")){
            cs = new SlowCooling();
        }
        else if(coolingScheduleName.equals("Decreasing Rate")){
            cs = new DecreasingRateCooling();
        }
        else if(coolingScheduleName.equals("Increasing Rate")){
            cs = new IncreasingRateCooling();
        }
        else{
            System.out.println("Error: Unable to parse Cooling Schedule");
        }
        return cs;
    }

    private AcceptanceFunction textConvertAcceptanceFunction(String acceptanceFunctionName) {
        AcceptanceFunction af = new MockAcceptance();

        if(acceptanceFunctionName.equals("Basic")){
            af = new BasicAcceptance();
        }

        else if(acceptanceFunctionName.equals("Boltzmann")){
            af = new BoltzmanAcceptance();
        }
        else if(acceptanceFunctionName.equals("Mock")){
            af = new MockAcceptance();
        }
        else{
            System.out.println("Error: Unable to parse Acceptance Function");
        }
        
        
        return af;
    }

    public void writeToFile(Solution solution) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        int rowCount = 0;
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Student Name");
        header.createCell(1).setCellValue("Allocated Project");
        for (Project p : solution.getSolutionData().keySet()) {
            Student s = solution.getSolutionData().get(p);
            Row row = sheet.createRow(++rowCount);

            Cell cell = row.createCell(0);
            cell.setCellValue(s.getFullName());
            cell = row.createCell(1);
            cell.setCellValue(p.getTitle());

          }

        try (FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.home") + File.separator + "SASolution");
        ) {
            workbook.write(outputStream);
            workbook.close();
        }

    }

    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = solution;
    }

    public AcceptanceFunction getAcceptanceFunction() {
        return acceptanceFunction;
    }

    public void setAcceptanceFunction(AcceptanceFunction acceptanceFunction) {
        this.acceptanceFunction = acceptanceFunction;
    }

    public CoolingSchedule getCoolingSchedule() {
        return coolingSchedule;
    }

    public void setCoolingSchedule(CoolingSchedule coolingSchedule) {
        this.coolingSchedule = coolingSchedule;
    }

    public double getGpaWeight() {
        return gpaWeight;
    }

    public void setGpaWeight(double gpaWeight) {
        this.gpaWeight = gpaWeight;
    }

    public Solution getBestSolution() {
        return bestSolution;
    }

    public void setBestSolution(Solution bestSolution) {
        this.bestSolution = bestSolution;
    }

}