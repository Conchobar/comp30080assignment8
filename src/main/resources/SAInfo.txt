File Selection
Files must be presented in .xlsx format.

There are two options for file selection: Select a Student/Project/Professor combination, or a single file with all the requred information. 

The three file combination can be used with files found in the resource folder of this submission.

The single file should have column headers in the following format(from left to right):

Some variations of Student, Student Number and Proposerare required columns columns. GPA is optional. Failure to provide a file in this format will result in the programme not functioning.

Algorithm Parameters

Acceptance functions

Boltzmann
Uses the formula e^(-E/T)

Mock
Uses the formula 1/(1+(E/T))

Basic
Uses formula current temperature/max temperature. Ignores energy delta.


Cooling Schedules

SlowCooling
Starts at 100, decreasing bt 0.1 after each set of 100 iterations. By far the most time consuming cooling schedule.

FastCooling
Starts at 100, decreasing by 1 after each set of 10 iterations.

IncreasingRateCooling
Starts at 1000, with the rate of cooling starting at 1, and increasing by 1.5 times the rate after each set of iterations.

DecreasingRateCooling
Starts at 1000, with the temperature set to 0.75 of its previous value after each set of iterations. Stop after temperature drops  below 0.1, as it will never reach 0.


GPA Percentage
Decides the influence of GPA in the formula to determine the temperature of a solution.  If set to 0, GPA will not be taken into account. If set to 100%, GPA will be the sole means of determining the fitness of a solution.
Default: 50%


Display Solution Before/After SA
Shows Bar Charts of before and after the programme executes, showing the distribution of projects based on student's preference list.

Write to File
Available after execution of the programme, this allows the user to save the Student-Project pairing considered best by the programme. It is recommended that the file is saved in the .xlsx format.
