File Selection
Files must be presented in .xlsx format.

There are two options for file selection: Select a Student/Project/Professor combination, or a single file with all the requred information. 

The three file combination can be used with files found in the resource folder of this submission.

The single file should have column headers in the following format(from left to right):

Some variations of Student, Student Number and Proposerare required columns columns. GPA is optional. Failure to provide a file in this format will result in the programme not functioning.

Algorithm Parameters

Sample Size
Determines the initial pool of candidate solutions to be generated at the start of the programme
Default: 1,000

Mate Percentage
Determines the top percentage of cadidates(in order of fitness) to be mated with each other each generation. Percentage is of the number of candidates after a cull is performed. 
Example: If Mate Percentage is 20%, there are 1,000 candidates and 400 are culled, 120 candidates will be selected to mate((1000-400)*0.2).
Default: 20%

Cull Percentage
Determines the number of candidates to be removed from the bottom of the candidate list(ordered by fitness).
Default: 20%

Mutate Percentage
The percentage of candidates to have random changes applied to their solution(while still remaining valid) after mating has occurred.
Default: 10%

Split Percentage
During crossover between two candidate - c1 and c2 - this percentage determines how much information should be taken from c1.
Default: 50%

GPA Percentage
Decides the influence of GPA in the formula to determine fitness of a solution.  If set to 0, GPA will not be taken into account. If set to 100%, GPA will be the sole means of determining the fitness of a solution.
Default: 50%


Display Solution Before/After GA
Shows Bar Charts of before and after the programme executes, showing the distribution of projects based on student's preference list.

Write to File
Available after execution of the programme, this allows the user to save the Student-Project pairing considered best by the programme. It is recommended that the file is saved in the .xlsx format.



